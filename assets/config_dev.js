angular.module('exchange.conf',[]).constant('UTIL_CONF', {
  middle_server     : 'https://api.developblockchain.net/',
  datapush_server   : 'https://datapush.developblockchain.net/',
  mainpage_data_url : '/state/subscribe?param=STATE',
  mainpage_event    : 'STATE',
  etherscanUrl      : 'https://kovan.etherscan.io/tx/',
  btcscanUrl        : 'https://www.blocktrail.com/tBTC/tx/',
  btcnet            : 'testnet', //mainnet
  PRICE_TRICKER     : 'https://api.coinmarketcap.com/v2/ticker/?limit=10',

  CONSTANT          : {
                        EOS                     : 'EOS',
                        BTC                     : 'BTC',
                        BTCTestnet              : 'BTCTestnet',
                        ETH                     : 'ETH',
                        ELOT                    : 'ELOT',
                        WITHDRAW_URI            : 'exchangebalance/withdraw',
                        DEPOSIT_URI             : 'exchangebalance/deposit',
                        ETH_BALANCE_URI         : 'transfer/ethbalance',
                        BTC_BALANCE_URI         : 'transfer/btcbalance',
                        ELOT_BALANCE_URI        : 'transfer/tokenbalance',
                        EOS_BALANCE_URI         : 'transfer/tokenbalance',
                        CONFIG_PAIRS_URI        : 'config/pairs',
                        TRADE_CANCEL_URI        : 'trade/cancel',
                        TRADE_PUTLIMIT_URI      : 'trade/putlimit',
                        IMPORT_WALLET_URI       : 'users/importbip39accountsbymnemonic',
                        CREAT_WALLET_URI        : 'users',
                        MARKET_URI              : 'market',
                        USER_URI                : 'data',
                        USER_BALANCE_URI        : 'trade/getBalance',
                        MARKET_SUBSCRIBE_URI    : 'market/subscribe?param=',
                        USER_SUBSCRIBE_URI      : 'data/subscribe?param=',
                        WITHDRAW_SUBMIT_SUCCESS : 'submit_success',
                        ETH_TRANSFER_URI        : 'transfer/ethtransfer',
                        TOKEN_TRANSFER_URI      : 'transfer/tokentransfer',
                        BTC_TRANSFER_URI        : 'transfer/btctransfer',
                        QRCODE_URI              : 'transfer/qrcode',
                        PRICE_TRICKER_URI       : 'transfer/getticker',
                        TRANSACTION_URI         : 'exchangebalance/asset_tx',
                        CAPTCHAPNG_URI          : 'Captchapng',
                        VERIFYIMG_URI           : '/verifyImg'
                      },
      ethGasprice   :'10',
      btcFee        :'0.0001',
      marketDecimal : {
                        EOSBTC                  : { EOS:2,BTC:6},
                        EOSETH                  : { EOS:2,ETH:6 },
                        ELOTETH                 : { ELOT:0,ETH:8 },
                        ELOTBTC                 : { ELOT:0,BTC:8 },
                        ETHBTC                  : { ETH:3,BTC:5 }
                       },
      scanner       : { EOS :'https://kovan.etherscan.io/tx/',
                        ELOT:'https://kovan.etherscan.io/tx/',
                        ETH :'https://kovan.etherscan.io/tx/',
                        BTC :'https://www.blocktrail.com/tBTC/tx/'
                      }
}
);
