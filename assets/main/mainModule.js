var mainModule = angular.module('mainModule', [
    'exchange.conf'
  , 'ngResource'
  , 'dataModule'
  , 'ngSessionStorage'
  , 'headerModule'
  , 'marketModule'
  , 'pascalprecht.translate'
  ]);

mainModule.controller('mainController', [
  '$scope',
  '$translate',
  function (
      $scope,
      $translate
  ) {

      $scope.container = {};
      $scope.container.params = {};
      $scope.container.market = {};
      $scope.container.user   = {};
      $scope.container.user.email         = "eric@icloud.com";
      $scope.container.user.traderBalance = {};
      $scope.container.user.openOrders    = [];
    
      
      bootbox.addLocale('ko', {
          OK: '승인',
          CANCEL: '취소',
          CONFIRM: '확인'
      });

      $scope.switch_language = function (lang) {
          console.log(lang)
          $translate.use(lang);
          window.localStorage.lang = lang;
          $scope.c_lang = $translate.use(lang);
          bootbox.setLocale(lang);
      };
      $scope.c_lang = $translate.use();
  }
]);



mainModule.directive('marketPriceChart', function () {
    return {
        templateUrl: 'main/marketPriceChart/marketPriceChart.html'
    };
});
mainModule.directive('tradeHistory', function () {
    return {
        templateUrl: 'main/tradeHistory/tradeHistory.html'
    };
});

mainModule.directive('balance', function () {
    return {
        templateUrl: 'main/balance/balance.html'
    };
});


mainModule.directive('openOrders', function () {
    return {
        templateUrl: 'main/openOrders/openOrders.html'
    };
});

mainModule.directive('tradedOrders', function () {
    return {
        templateUrl: 'main/tradedOrders/tradedOrders.html'
    };
});

mainModule.directive('orderBook', function () {
    return {
        templateUrl: 'main/orderBook/orderBook.html'
    };
});
mainModule.directive('orderBook2', function () {
    return {
        templateUrl: 'main/orderBook2/orderBook.html'
    };
});


mainModule.directive('exchageHeader', function () {
    return {
        templateUrl: 'main/header/header.html'
    };
});


mainModule.directive('deposite', function () {
    return {
        templateUrl: 'main/login/deposit.html'
    };
});
mainModule.directive('withdraw', function () {
    return {
        templateUrl: 'main/login/withdraw.html'
    };
});

mainModule.directive('exchange', function () {
    return {
        templateUrl: 'main/login/exchange.html'
    };
});

mainModule.directive("limitTo", [function () {
    return {
        restrict: "A",
        link: function (scope, elem, attrs) {
            if (-1 != attrs.limitTo.indexOf(",")) {
                var p = parseInt(attrs.limitTo.split(',')[0], 10);
                var s = parseInt(attrs.limitTo.split(',')[1], 10);

                angular.element(elem).on("keypress", function (e) {
                    if (e.keyCode != 8 // backspace) 
                    ) {
                        if( s == 0 && -1 != $(this).val().indexOf(".")){
                            e.preventDefault();
                        } else if ($(this).val()) {
                            if (!/^\d|\.$/.test(String.fromCharCode(e.charCode))) { e.preventDefault(); }
                            var hasDot = !(-1 == $(this).val().indexOf("."));
                            var count = ($(this).val().match(/\./g) || []).length;
                            var vp = $(this).val().split('.')[0];
                            var vs = $(this).val().split('.')[1];
                            if (count >= 1 && String.fromCharCode(e.charCode) == ".") {
                                e.preventDefault();
                            }
                            if (!hasDot && (vp && vp.length >= p)) {
                                e.preventDefault();
                            }
                            if (hasDot && ((vp && vp.length > p) || (vs && vs.length >= s))) {
                                e.preventDefault();
                            }

                        }else {
                            if (!/^\d$/.test(String.fromCharCode(e.charCode))) { e.preventDefault(); }
                        }
                    }
                });
            }
        }
    }
}]);


mainModule.config(['$compileProvider', function ($compileProvider) {
    $compileProvider.debugInfoEnabled(false);
}]);

mainModule.config(['$translateProvider', function ($translateProvider) {
    var lang = window.localStorage.lang;
    if ((lang != "en") && (lang != "zh_CN") && (lang != "ja") && (lang != "ko")) {
        lang = "en";
    }
    //   var lang = window.localStorage.lang||'en';
    $translateProvider.preferredLanguage(lang);
    $translateProvider.useStaticFilesLoader({
        prefix: 'i18n/',///语言包路径
        suffix: '.json'
    });
    $translateProvider.fallbackLanguage(lang);
    //   $translateProvider.useSanitizeValueStrategy('sce');
    $translateProvider.translations("en", {
        "Select Pairs": "Select Pairs",
        "Last trade price": "Last trade price",
        "24h Change": "24h Change",
        "Select language": "Select language",

        "Balance": "Balance",
        "Buy": "Buy",
        "Sell": "Sell",
        "Amount": "Amount",
        "Price": "Price",
        "BUY": "BUY",
        "SELL": "SELL",
        "Market Price Chart": "Market Price Chart",
        "Buy Order Book": "Buy Order Book",
        "Sell Order Book": "Sell Order Book",
        "My Orders": "My Orders",

        "Deposit": "Deposit",

        "Your Exchange Record": "Trade History",
        "Exchange Data": "Exchange Data",

        "Sign in to your Wallet": "Sign in to your wallet",
        "Exchange Center IS A DECENTRALIZED EXCHANGE BUILT ON ETHEREUM": "Exchange Center IS A DECENTRALIZED EXCHANGE BUILT ON ETHEREUM",

        "Created wallet": "Created wallet",
        "Withdraw": "Withdraw",
        "Open Orders": "Open Orders",
        "Date": "Time",
        "Type": "Type",
        "Amount": "Amount",
        "Filled": "Filled",
        "Price": "Price",
        "Total": "Total",
        "Cancel": "Cancel",
        "Cancelled": "Cancelled",
        "Order Book": "Order Book",
        "New Wallet": "New wallet",
        "Import wallet": "Import wallet",
        "Export private key": "Export private key",
        "Clear Wallet": "Clear Wallet",
        "Deposit": "Deposit",
        "Withdraw": "Withdraw",
        "Transaction History": "Transaction History",
        "Gas price": "Gas price",

        "Trade History": "Trade History",
        "Market History": "Market History",
        "My History": "My History",

        "Trade History": "Trade History",
        "ERC/ETH": "ERC/ETH",
        "Market History": "Market History",
        "My History": "My History",
        "Amount to buy": "Amount to buy",
        "Your pay": "Your pay",

        // popup new wallet
        "Create your Exchange Center Wallet": " Create your Exchange Center Wallet",
        "WELCOME TO Exchange Center": "WELCOME TO Exchange Center",
        "It Is A Decentralized Exchange Built On Ethereum": "It Is A Decentralized Exchange Built On Ethereum",
        "Passwords must be at least 8 characters": "Passwords must be at least 8 characters",
        "Repeat wallet password": "Repeat wallet password",
        "The wallet is created successfully": "The wallet is created successfully",
        "You just created an Exchange Center account:": "You just created an Exchange Center account:",
        "Please BACKUP the private key for this account:": "Please BACKUP the private key for this account:",
        //clear wallet
        "No wallet has been created yet": "No wallet has been created or imported.",
        "Please confirm the wallet private key has been BACKUP, will not be restored.": "Please make sure your private key has been backed up, while Exchange Center won't keep it in any way. ",
        //new wallet
        "One wallet has existed，pleas clear wallet": "One wallet has already been existed, please clear wallet first.",
        "Please enter your wallet password": "Please enter your wallet password.",
        "Passwords must be at least 8 characters": "Passwords must be at least 8 characters.",
        "Password does not equals repeated password": "Two password inputs do not match.",
        "Creating wallet": "Creating wallet",
        "The wallet is created successfully": "The wallet has been created successfully.",
        "You just created an Exchange Center account": "You just created an Exchange Center account.",
        //outputwallet
        "Please input Password": "Please input Password",
        "Please enter your wallet password": "Please enter your wallet password",
        "Private Key is null, please import wallet or create wallet": "Private Key is null, please import wallet or create wallet",
        "Please remember your address and private key": "Please remember your address and private key",
        //importWallet
        "Please enter your wallet private key": "Please enter your wallet private key.",
        "Please enter your wallet password": "Please enter your wallet password.",
        "Passwords must be at least 8 characters": "Passwords must be at least 8 characters.",
        "Password does not equals repeated password": "Two password inputs do not match.",
        "Importing wallet": "Importing wallet",
        "Wallet Address:": "Wallet address:",
        //withdraw
        "Please input withdraw amount": "Please input withdrawl amount.",
        "Please input password": "Please enter your wallet password.",
        "Please enter your wallet password": "Please enter your wallet password.",
        "Please input withdraw amount": "Please input withdrawl amount.",
        "Withdraw asset successfully": "Withdrawl has been completed.",
        "Please appoint asset": "Please appoint asset.",
        "DEPOSIT": "DEPOSIT",
        "WITHDRAW": "WITHDRAW",
        "REQUESTING": "REQUESTING",
        "APPROVING": "APPROVING",
        "APPROVED": "APPROVED",
        "APPROVEFAILED": "APPROVEFAILED",
        "EXCUTING": "EXCUTING",
        "EXCUTEFAILED": "EXCUTEFAILED",
        "COMPLETED": "COMPLETED",
        "REJECT": "REJECT",
        //Deposit
        "Deposit data successfully": "Deposit has been completed.",
        "Deposit error": "Deposit error",
        "balance not enough": "balance not enough",
        "wallet balance": "wallet balance",
        //buy/sell
        "Are you sure to cancel this order?": "Are you sure to cancel this order?",
        //common
        "operation failed": "operation failed"

    });
    $translateProvider.translations("zh_CN", {
        "Select Pairs": "选择货币对",
        "Last trade price": "最后成交价格",
        "24h Change": "24h 涨幅",
        "Select language": "选择语言",

        "Balance": "交易所余额",
        "Buy": "买入",
        "Sell": "卖出",
        "BUY": "买入",
        "SELL": "卖出",
        "Trade History": "成交记录",
        "Market History": "市场成交记录",
        "My History": "我的成交记录",
        "Market Price Chart": "市场K线图",
        "Buy Order Book": "买入深度",
        "Sell Order Book": "卖出深度",
        "My Orders": "我的订单",

        "New Wallet": "创建钱包",
        "Import wallet": "导入钱包",
        "Export private key": "导出钱包私钥",
        "Clear Wallet": "清除钱包",
        "Transaction History": "充值&提币记录",
        "Deposit": "充值",
        "Gas price": "Gas价格",

        "Your Exchange Record": "充值&提币记录",
        "Exchange Data": "交易日期",
        "Please input your private key": "输入钱包私钥",
        "Import in to your Wallet": "导入钱包",
        "Exchange Center IS A DECENTRALIZED EXCHANGE BUILT ON ETHEREUM": "Exchange Center是基于以太网去中心化交易平台",


        "Create wallet": "创建钱包",
        "Withdraw": "提币",
        "Open Orders": "我的挂单",
        "Date": "时间",
        "Type": "买/卖",
        "Amount": "数量",
        "Filled": "成交数量",
        "Price": "单价",
        "Total": "总价",
        "Cancel": "撤单",
        "Cancelled": "已撤单",
        "Order Book": "市场深度",
        "Amount to buy": "数量",
        "Your pay": "合计",

        // popup new wallet
        "Create your Exchange Center Wallet": "创建您的Exchange Center钱包",
        "WELCOME TO Exchange Center": "欢迎使用 Exchange Center",
        "It Is A Decentralized Exchange Built On Ethereum": "基于以太坊的去中心化交易所",
        "Passwords must be at least 8 characters": "钱包密码至少8位",
        "Repeat wallet password": "确认密码",
        "The wallet is created successfully": "成功创建钱包",
        "You just created an Exchange Center account:": "您已成功创建了Exchange Center钱包，钱包地址：",
        "Please BACKUP the private key for this account:": "请您务必保存好钱包私钥：",
        //clear wallet
        "No wallet has been created yet": "您还没有创建钱包。",
        "Please confirm the wallet private key has been BACKUP, will not be restored.": "请保管好您的钱包私钥，清除后不可恢复。",
        //new wallet
        "One wallet has existed，pleas clear wallet": "请先清除现有钱包。",
        "Please enter your wallet password": "请输入您的钱包密码",
        "Passwords must be at least 8 characters": "钱包密码至少8位",
        "Password does not equals repeated password": "密码不一致，请重新输入",
        "Creating wallet": "创建钱包",
        "The wallet is created successfully": "成功创建钱包",
        "You just created an Exchange Center account": "您成功创建了Exchange Center钱包。",
        //outputwallet
        "Please input Password": "请输入钱包密码",
        "Please enter your wallet password": "请输入您的钱包密码",
        "Private Key is null, please import wallet or create wallet": "钱包私钥为空，请先创建或者导入钱包",
        "Please remember your address and private key": "请保管好您的钱包地址和私钥",
        //importWallet
        "Please enter your wallet private key": "请输入您的钱包私钥",
        "Please enter your wallet password": "请输入钱包密码",
        "Passwords must be at least 8 characters": "钱包密码至少8位",
        "Password does not equals repeated password": "密码不一致，请重新输入",
        "Importing wallet": "导入钱包",
        "Wallet Address:": "钱包地址:",
        //withdraw
        "Please input withdraw amount": "请输入提币数量",
        "Please input password": "请输入钱包密码",
        "Please enter your wallet password": "请输入您的钱包密码",
        "Please input withdraw amount": "请输入提币数量",
        "Withdraw asset successfully": "提币成功",
        "Please appoint asset": "请选择币种.",
        "DEPOSIT": "入金",
        "WITHDRAW": "出金",
        "REQUESTING": "申请中",
        "APPROVING": "确认中",
        "APPROVED": "已确认",
        "APPROVEFAILED": "确认中",
        "EXCUTING": "Pending",
        "EXCUTEFAILED": "失败",
        "COMPLETED": "已完成",
        "REJECT": "申请中",
        //Deposit
        "Deposit data successfully": "充值成功",
        "Deposit error": "充值失败",
        "balance not enough": "账户余额不足",
        "wallet balance": "钱包余额",
        //buy/sell
        "Are you sure to cancel this order?": "确定要撤销此订单吗?",
        //common
        "operation failed": "操作失败"

    });
    $translateProvider.translations("ja", {
        "Select Pairs": "コインペア選択",
        "Last trade price": "最終取引価格",
        "24h Change": "24時間変動",
        "Select language": "言語選択",

        "Balance": "取引所残高",
        "Buy": "買い",
        "Sell": "売り",
        "BUY": "買い",
        "SELL": "売り",
        "Trade History": "取引履歴",
        "Market History": "取引所取引履歴",
        "My History": "私の取引履歴",
        "Market Price Chart": "マーケットチャート",
        "Buy Order Book": "買オーダーリスト",
        "Sell Order Book": "売オーダーリスト",
        "My Orders": "マイオーダー",

        "New Wallet": "新しいウォレット",
        "Import wallet": "ウォレットのインポート",
        "Export private key": "秘密キーのエクスポート",
        "Clear Wallet": "ウォーレットのクリア",
        "Transaction History": "入出金履歴",
        "Deposit": "入金",

        "Your Exchange Record": "入出金履歴",
        "Exchange Data": "取引日付",

        "Import in to your Wallet": "財布のインポート",
        "Exchange Center IS A DECENTRALIZED EXCHANGE BUILT ON ETHEREUM": "Exchange CenterはEthereumプラットフォームに基づく分散型の取引所です",

        "Create wallet": "ウォーレット作成",
        "Withdraw": "出金",
        "Open Orders": "オープンオーダー",
        "Date": "時間",
        "Type": "買/売",
        "Amount": "数量",
        "Filled": "約定済",
        "Price": "単価",
        "Total": "合計",
        "Cancel": "取消",
        "Cancelled": "取消済み",
        "Order Book": "オーダーブック",
        "Gas price": "ガスプライス",
        "Amount to buy": "購入数量",
        "Your pay": "合計",

        // popup new wallet
        "Create your Exchange Center Wallet": "あなたのExchange Centerウォレットを作る",
        "WELCOME TO Exchange Center": "ようこそExchange Centerへ",
        "It Is A Decentralized Exchange Built On Ethereum": "It Is A Decentralized Exchange Built On Ethereum",
        "Passwords must be at least 8 characters": "半角英数８文字",
        "Repeat wallet password": "パスワード確認",
        "The wallet is created successfully": "ウォレットが作成されました",
        //clear wallet
        "No wallet has been created yet": "まだウォレットは作成されていません。",
        "Please confirm the wallet private key has been BACKUP, will not be restored.": "ウォレットの秘密鍵がバックアップされていることを確認してください。リストアできません。",
        //new wallet
        "One wallet has existed，pleas clear wallet": "ウォレットがすでに存在します。先にウォレットをクリアしてください。",
        "Please enter your wallet password": "ウォレットのパスワードを入力してください。",
        "Passwords must be at least 8 characters": "8文字以上のパスワードを入力してください。",
        "Password does not equals repeated password": "パスワードが確認用パスワードと一致しません。",
        "Creating wallet": "ウォレットをクリアする。",
        "The wallet is created successfully": "ウォレットが正しく作成されました。",
        "You just created an Exchange Center account": "Exchange Centerアカウントを作成しました。",
        //outputwallet
        "Please input Password": "パスワードを入力してください。",
        "Private Key is null, please import wallet or create wallet": "秘密鍵が設定されていません。ウォレットをインポートするか新しいウォレットを作成してください。",
        "Please remember your address and private key": "ウォレットアドレスと秘密鍵をメモしておいてください。",
        //importWallet
        "Please enter your wallet private key": "ウォレットの秘密鍵を入力してください。",
        "Importing wallet": "ウォレットをインポートしています。",
        "Wallet Address:": "ウォレットアドレス:",
        //withdraw
        "Please input withdraw amount": "出金するコイン数量を入力してください。",
        "Please input password": "パスワードを入力してください。",
        "Withdraw asset successfully": "コインの出金に成功しました。",
        "Please appoint asset": "コインを指定してください。",
        "DEPOSIT": "入金",
        "WITHDRAW": "出金",
        "APPROVING": "承認中",
        "APPROVED": "承認済",
        "APPROVEFAILED": "承認失敗",
        "EXCUTING": "実行中",
        "EXCUTEFAILED": "実行失敗",
        "COMPLETED": "完了",
        "REJECT": "REJECT",
        //Deposit
        "Deposit data successfully": "コインの入金に成功しました。",
        "Deposit error": "入金失敗",
        "balance not enough": "残高が足りません。",
        "wallet balance": "財布残高",
        //buy/sell
        "Are you sure to cancel this order?": "このオーダーを取消しますか?",
        //common
        "operation failed": "処理が失敗しました。"

    });
    $translateProvider.translations("ko", {
        "Select Pairs": "코인패어선택",
        "Last trade price": "현재가",
        "24h Change": "24시간 변동",
        "Select language": "언어선택",

        "Balance": "잔고",
        "Buy": "매수",
        "Sell": "매도",
        "Amount": "수량",
        "Price": "가격",
        "BUY": "매수",
        "SELL": "매도",
        "Market Price Chart": "시장 가격 차트",
        "Buy Order Book": "Buy Order Book",
        "Sell Order Book": "Sell Order Book",
        "My Orders": "My Orders",

        "Your Exchange Record": "고객님의 거래내용",
        "Exchange Data": "거래 데이터",

        "sign in to your Wallet": "당신의 지갑으로 로그인하다",
        "Exchange Center IS A DECENTRALIZED EXCHANGE BUILT ON ETHEREUM": "  Exchange Center는 이더리움에 기초한 분산화 거래소입니다",

        "Create your Exchange Center Wallet": "당신의 Exchange Center 지갑을 만들다",
        "WELCOME TO Exchange Center": "Exchange Center에 오신것을 환영합니다",
        "It Is A Decentralized Exchange Built On Ethereum": "이더리움에 기초한 분산화 거래소입니다",

        "Create wallet": "지갑을 만들다",
        "Withdraw": "출금",
        "Market Price Char": "시장 가격 차트",
        "Open Orders": "오픈 오더",
        "Date": "시간",
        "Type": "종류",
        "Amount": "수량",
        "Filled": "약정수량",
        "Price": "가격",
        "Total": "합계",
        "Cancel": "취소",
        "Cancelled": "취소완료",
        "Order Book": "오더북",
        "New Wallet": "새 지갑",
        "Import wallet": "수입 지갑",
        "Export private key": "수출 개인 키",
        "Clear Wallet": "잘 지갑",
        "Deposit": "입금",
        "Withdraw": "철수",
        "Transaction History": "예금 & 철수 목록",
        "Gas price": "가스",

        "Trade History": "무역의 역사",
        "Market History": "시장 역사",
        "My History": "나는 역사",

        // popup new wallet
        "Create your Exchange Center Wallet": "당신의 Exchange Center 지갑을 만들다",
        "WELCOME TO Exchange Center": "Exchange Center 에 오신것을 환영합니다",
        "It Is A Decentralized Exchange Built On Ethereum": "이더리움에 기초한 분산화 거래소입니다",
        "Passwords must be at least 8 characters": "8 자 이상의 암호를 입력하십시오.",
        "Repeat wallet password": "Repeat wallet password",
        "The wallet is created successfully": "지갑이 성공적으로 생성되었습니다",
        //clear wallet
        "No wallet has been created yet": "아직 지갑은 작성되지 않습니다.",
        "Please confirm the wallet private key has been BACKUP, will not be restored.": "지갑의 비밀 키가 백업되어 있는지 확인하십시오. 복원 할수 없습니다.",
        //new wallet
        "One wallet has existed，pleas clear wallet": "지갑이 이미 존재합니다. 먼저 지갑을 취소하십시오.",
        "Please enter your wallet password": "지갑의 암호를 입력하십시오.",
        "Passwords must be at least 8 characters": "8 자 이상의 암호를 입력하십시오.",
        "Password does not equals repeated password": "암호가 확인암호와 일치하지 않습니다.",
        "Creating wallet": "지갑을 취소하고 있습니다.",
        "The wallet is created successfully": "지갑이 제대로 작성되었습니다.",
        "You just created an Exchange Center account": "Exchange Center 계정을 만들었습니다.",
        //outputwallet
        "Please input Password": "암호를 입력하십시오.",
        "Private Key is null, please import wallet or create wallet": "비밀 키가 설정되어 있지 않습니다. 지갑을 가져 오거나 새로운 지갑을 작성하십시오.",
        "Please remember your address and private key": "지갑 주소와 비밀 키를 메모 해 두십시오",
        //importWallet
        "Please enter your wallet private key": "지갑의 비밀 키를 입력하십시오.",
        "Please enter your wallet password": "지갑의 암호를 입력하십시오.",
        "Passwords must be at least 8 characters": "8 자 이상의 암호를 입력하십시오.",
        "Password does not equals repeated password": "암호가 확인암호와 일치하지 않습니다.",
        "Importing wallet": "지갑을 가져 오고 있습니다.",
        "Wallet Address:": "Wallet address:",
        //withdraw
        "Please input withdraw amount": "출금 코인 수량을 입력하십시오",
        "Please input password": "암호를 입력하십시오.",
        "Withdraw asset successfully": "코인의 출금에 성공했습니다.",
        "Please appoint asset": "코인을 지정하십시오.",
        "DEPOSIT": "DEPOSIT",
        "WITHDRAW": "WITHDRAW",
        "REQUESTING": "REQUESTING",
        "APPROVING": "APPROVING",
        "APPROVED": "APPROVED",
        "APPROVEFAILED": "APPROVEFAILED",
        "EXCUTING": "EXCUTING",
        "EXCUTEFAILED": "EXCUTEFAILED",
        "COMPLETED": "COMPLETED",
        "REJECT": "REJECT",
        //Deposit
        "Deposit data successfully": "코인 입금에 성공했습니다.",
        "Deposit error": "Deposit error",
        "balance not enough": "balance not enough",
        "wallet balance": "wallet balance",
        //buy/sell
        "Are you sure to cancel this order?": "Are you sure to cancel this order?",
        //common
        "operation failed": "처리가 실패했습니다."

    });

}]);
