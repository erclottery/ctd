var marketModule = angular.module('marketModule', [
    'ngResource', 'ngSessionStorage'
]);

marketModule.controller('marketController', ['$scope',function($scope) {

  var buttonNumer=7;

  $scope.$on('event:loadFinish', function (event, arg) { 
    
    if (arg == "initfinish") 
    {
        loadDataToChart($scope.container.market.market);
        initDropList();
    }else {
        $scope.chart.series[0].addPoint($scope.container.market.market[0]);
        $scope.chart.series[1].addPoint([$scope.container.market.market[0][0],$scope.container.market.market[0][5]]);

    }
  });

  Highcharts.setOptions({ global: { useUTC: false } });
        function GetDateStr(AddDayCount) {
      var dd = new Date(); 
      dd.setDate(dd.getDate()+AddDayCount);
      return dd.getTime(); 
  }

  function setMarketParams(time,interval) {
    
     $scope.container.params.start      = GetDateStr(-time);
     $scope.container.params.end        = new Date().getTime();
     $scope.container.params.interval   = interval;
     $scope.fetchMarketData();
    

}




function initDropList(){
    ////////////动画/////////////

    //Author: Brady Sammons
    //URL: www.bradysammons.com
    /* -------------------------------------------------------- */
    /*	//set Global variables
    /* -------------------------------------------------------- */
    var cards = $(".card-drop"),
    toggler = cards.find(".toggle"),
    links = cards.find("ul>li>a"),
    li = links.parent('li'),
    count = links.length,
    width = links.outerWidth();

    //set z-Index of drop Items
    links.parent("li").each(function (i) {
    $(this).css("z-index", count - i); //invert the index values
    });

    //set top margins & widths of li elements
    function setClosed() {
    li.each(function (index) {
    $(this).css("top", index * 2)
    .css("width", 200 - index * 2)
    // .css("display" , 'none')
    .css("margin-left", (index * 2) / 2);
    });
    $('#ul').animate({ display: 'none' }, 500)
    li.addClass('closed');
    toggler.removeClass("active");
    }
    setClosed();

    /* -------------------------------------------------------- */
    /*	Toggler Click handler
    /* -------------------------------------------------------- */
    toggler.on("mousedown", function () {
    var $this = $(this); //cache $(this)
    var $ul = $('#ul');
    //if the menu is active:


    if ($this.is(".active")) {
    setClosed();
    } else {
    //if the menu is un-active:
    $this.addClass("active");
    li.removeClass('closed');
    //set top margins
    li.each(function (index) {
    $(this).css("top", 60 * (index + 1))
        .css("width", "200")
        .css("margin-left", "0px")
        .css("text-align", "center")
        .css("line-height", "60px");
    // .css("display" , "block");
    $('#ul').animate({ display: 'block' }, 500)

    });

    // $('#ul').addClass('dis-noe')
    }


    });

    /* -------------------------------------------------------- */
    /*	Links Click handler
    /* -------------------------------------------------------- */
    links.on("click", function (e) {
    // alert(2)

    var $this = $(this),
    label = $this.data("label");
    icon = $this.children("i").attr("class");

    li.removeClass('active');
    if ($this.parent("li").is("active")) {
    $this.parent('li').removeClass("active");
    } else {
    $this.parent("li").addClass("active");
    }
    toggler.children("span").text(label);
    toggler.children("i").removeClass().addClass(icon);
    setClosed();
    e.preventDefault;
    });
}

Highcharts.setOptions({
    lang: {
        rangeSelectorZoom: ''    // hidden the text 'Zoom' show in chart
    }
    })

function loadDataToChart(data){

    var ohlc = [],
    volume = [],
    dataLength = data.length,
   
    groupingUnits = [
        [
            'week', // unit name
            [1] // allowed multiples
        ],
        [
            'day', // unit name
            [1] // allowed multiples
        ],
        [
            'hour', // unit name
            [1, 2, 4] // allowed multiples
        ],
        [
            'minute', [ 3, 5, 15, 30]
        ]
    ],

    i = 0;

for (i; i < dataLength; i += 1) {
    ohlc.push([
        data[i][0], // the date
        data[i][1], // open
        data[i][3], // high
        data[i][4], // low
        data[i][2] // close
    ]);

    volume.push([
        data[i][0], // the date
        data[i][5] // the volume
    ]);
}

// create the chart
$scope.chart = Highcharts.stockChart('container', {
    
    chart: {
        backgroundColor: '#122028',
    },

    xAxis: {
    crosshair: {
        snap: false
      },
      events: {
        setExtremes: function(e) {
          if(typeof(e.rangeSelectorButton)!== 'undefined') {
            var count = e.rangeSelectorButton.count;
            var type = e.rangeSelectorButton.type;
            var text = e.rangeSelectorButton.text;
            var btn_index = null;
            if(text =="1m" && type == "minute"){
                buttonNumer=0;
                setMarketParams(1,60);
            } else if(text =="5m" && type == "minute"){
                buttonNumer=1;
                setMarketParams(5,300);
            } else if(text =="15m" && type == "minute"){
                buttonNumer=2;
                setMarketParams(5,900);
            } else if (text =="30m" && type == "minute"){
                buttonNumer=3;
                setMarketParams(5,1800);
            } else if(text =="1h" && type == "hour"){
                buttonNumer=4;
                setMarketParams(5,3600);
            } else if(text =="2h" && type == "hour"){
                buttonNumer=5;
                setMarketParams(5,7200);
            } else if(text =="4h" && type == "hour"){
                buttonNumer=6;
                setMarketParams(10,14400);
            } else if(text =="1d" && type == "day"){
                buttonNumer=7;
                setMarketParams(360,86400);
            } else if(text =="1w" && type == "week"){
                buttonNumer=8;
                setMarketParams(360,604800);
            }
          }
        }
      },
      dateTimeLabelFormats: {
        millisecond: '%H:%M:%S.%L',
        second: '%H:%M:%S',
        minute: '%H:%M',
        hour: '%H:%M',
        day: '%m-%d',
        week: '%m-%d',
        month: '%y-%m',
        year: '%Y'
    }
    },
    yAxis: [{
        gridLineWidth:1,
        gridLineColor:"#26343C",
        min:0, // 定义最小值
        //tickPixelInterval:10,
        allowDecimals:true,
        crosshair: {
            snap: false,
            width: 1
        },
        labels: {
            align: 'left',
            x: 1
        },
        // title: {
        //     text: 'OHLC'
        // },
        height: '60%',
        lineWidth: 1,
        resize: {
            enabled: true
        }
    }, {
        gridLineWidth:1,
        gridLineColor:"#26343C",
        min:0, // 定义最小值 
        tickPixelInterval:10,
        allowDecimals:true,
        crosshair: {
            snap: false,
            width: 1
        },
        labels: {
            align: 'left',
            x: 1
        },
        // title: {
        //     text: 'Volume'
        // },
        top: '65%',
        height: '35%',
        offset: 0,
        lineWidth: 2
    }],
    rangeSelector: {
        allButtonsEnabled: true,
        //inputDateFormat: '%Y-%m-%d',
        selected: buttonNumer,
        inputEnabled: false ,
        buttonTheme: { 
            fill: '#26343C', 
            stroke: 'none', 
            'stroke-width': 0, 
            r: 8, 
            states: { 
                hover: { 
                    fill:'#000000',                                                   
                }, 
                select: { 
                    fill: '#000000', 
                    style: { 
                        color: '#0f8dbc' 
                    } 
                } 
            } 
        },
        labelStyle: {
            color: 'silver',
            fontWeight: 'bold'
        },
        buttons: [{
            type: 'minute',
            count: 60,
            text: '1m'
        }, {
            type: 'minute',
            count: 300,
            text: '5m'
        }, {
            type: 'minute',
            count: 900,
            text: '15m'
        }, {
            type: 'minute',
            count: 1800,
            text: '30m'
        }, {
            type: 'hour',
            count: 60,
            text: '1h'
        }, {
            type: 'hour',
            count: 120,
            text: '2h'
        }, {
            type: 'hour',
            count: 240,
            text: '4h'
        }, {
            type: 'day',
            count: 15,
            text: '1d'
        }, {
            type: 'week',
            count: 0,
            text: '1w'
        }]
    },
    //crosshair: true,
    // tooltip: {
    //     split: true
    // },

    // plotOptions:{
    //     column:{
    //         pointWidth:5
    //     }
    // },

    plotOptions: {
        column:{
            pointWidth:5
        },
        series: {
            dataLabels: {
                color: '#B0B0B3'
            },
            marker: {
                lineColor: '#333'
            }
        },
        candlestick: {
            color: '#0f8dbc',
            upColor: 'red',
            lineColor: '#0f8dbc',
            upLineColor: 'red',
            maker: {
                states: {
                    hover: {
                        enabled: false,
                    }
                }
            }
        },
        boxplot: {
            fillColor: '#505053'
        },
        errorbar: {
            color: 'white'
        }
    },

    // series: [{
    //     type: 'candlestick',
    //     name: 'OHLC',
    //     data: ohlc,
    //     dataGrouping: {
    //         units: groupingUnits
    //     }
    // }, {
    //     type: 'column',
    //     name: 'Amount',
    //     data: volume,
    //     yAxis: 1,
    //     dataGrouping: {
    //         units: groupingUnits
    //     }
    // }]
    
    navigator: {
        xAxis: {
            tickWidth: 0,
            lineWidth: 0,
            gridLineWidth: 1,
            tickPixelInterval: 200,
            labels: {
                align: 'left',
                style: {
                    color: '#888'
                },
                x: 3,
                y: -4
            }
        }
    },
    
    // xAxis: {

    // },
    tooltip: {
        split: false,
        shared: true,
    },
    series: [{
        type: 'candlestick',
        name: 'OHLC',
        color: 'red',
        lineColor: 'red',
        upColor: 'transparent',
        upLineColor: 'green',
        pointWidth:5,
        pointPadding:1,
        tooltip: {
        },
        navigatorOptions: {
            color: Highcharts.getOptions().colors[0]
        },
        data: ohlc,
        pointStart: data.pointStart,
        pointInterval: data.pointInterval,
        // dataGrouping: {
        //     units: groupingUnits
        // },
        showInNavigator: true,
        id: 'sz'
    },{
        type: 'column',
        name: 'Amount',
        data: volume,
        yAxis: 1
        // dataGrouping: {
        //     units: groupingUnits
        // }
    }]
});

}
////////////////////////////////////////
//init end                        //////
////////////////////////////////////////
    }
]);

