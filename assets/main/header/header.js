var headerModule = angular.module('headerModule', ['ngResource', 'ngSessionStorage', 'ngStorage', 'exchange.conf']);

headerModule.controller('headerController', [
    '$scope',
    '$localStorage',
    '$http',
    '$window',
    '$location',
    '$filter',
    'UTIL_CONF',
    function (
        $scope,
        $localStorage,
        $http,
        $window,
        $location,
        $filter,
        UTIL_CONF
    ) {
        init();
        function init() {
            $scope.languages = [{ "value": 'en', "name": "English" }, { "value": 'ja', "name": "日本語" }, { "value": 'ko', "name": "한국어<" }, { "value": 'zh_CN', "name": "简体中文" }];
            $scope.c_lang    = window.localStorage.lang || 'en';
            bootbox.setLocale($scope.c_lang);

            if( $localStorage.user && $localStorage.user.wallet ){
                $scope.container.user               = $localStorage.user;
                $scope.container.user.Authorization = "Bearer " + $localStorage.user.token;
                $scope.container.user.islogin       = true;
                handleUserData();
                fetchCurrentBalance();
                initParam();
                $http.defaults.headers.common['Authorization'] = $scope.container.user.Authorization;
            }else{
                $scope.container.user.islogin = false;
            }

            console.log('container :',$scope.container);

            $http.defaults.headers.post['Content-Type']   = 'application/json; charset=utf-8';
            $http.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';

        }
        $scope.withdraw = function ( assetname , address ,  comment , size  ) {
            if ( !assetname ) {
                systemInfo("Please input asset", "operation failed");
                return;
            }

            if ( !address ) {
                systemInfo("Please input address", "operation failed");
                return;
            }

            if( ! size || size ===0 ){
                systemInfo("Please input size", "operation failed");
                return;
            }

            if ( !comment ) {
                comment = 'withdaw';
            }

            bootbox.prompt({
                title: $scope.pageTitle = $filter('translate')("Please input password"),
                inputType: 'password',
                callback: function (result) {
                    if (result) {
                        if( $scope.container.user.password == result ){
                            withdraw( assetname , address ,  comment , size  );
                        }else{
                            systemInfo("Your wallet password is not correct", "operation failed");
                            return;
                        }
                    }else if (result == '') {
                        systemInfo("Please enter your wallet password", "operation failed");
                        return;
                    }
                }
            });
        }
        function withdraw( assetname , address ,  comment , size  ) {
            var req = {
                method: 'POST',
                url: UTIL_CONF.middle_server + UTIL_CONF.CONSTANT.WITHDRAW_URI,
                data:  {
                         userid: $scope.container.user.userid ,
                        detail : comment ,
                     assetname : assetname,
                       address : address,
                          size : String(size)
                      }
               }

            $http(req).then(
                (resp)=>{
                   if ( resp.data.msg = UTIL_CONF.CONSTANT.WITHDRAW_SUBMIT_SUCCESS ){
                        bootbox.alert({
                            title: $scope.pageTitle = $filter('translate')("Submit withdraw application successfully"),
                            message: "<h1>" +$filter('translate')("Submit withdraw application successfully")+"</h1>",
                            callback: function (result) {}
                        });
                   }
            });
        }

        $scope.deposit = function (  assetname , address , comment , size , pk ) {

            if ( ! assetname ) {
                systemInfo("Please input deposit ", "operation failed");
                return;
            }

            if ( ! address ) {
                systemInfo("Please input address ", "operation failed");
                return;
            }

            if ( ! comment ) {
                systemInfo("Please input comment ", "operation failed");
                return;
            }

            if ( ! size ) {
                systemInfo("Please input size ", "operation failed");
                return;
            }

            if (  size <= 0 ) {
                systemInfo("Please input size larger then 0 ", "operation failed");
                return;
            }

            bootbox.prompt({
                title: $scope.pageTitle = $filter('translate')("Please input Password"),
                inputType: 'password',
                callback: function (result) {
                    if (result) {
                        if( $scope.container.user.password == result ){
                            deposit( assetname , address , comment , size , pk , $scope.container.user.wallet.ethGasprice, $scope.container.user.wallet.btcFee );
                        }else{
                            systemInfo("Password ERROR", "Please input correct password");
                        }

                    } else if (result == '') {
                        systemInfo("Please enter your wallet password", "operation failed");
                        return;
                    }

                }
            });
        }

        function deposit(  assetname , address , comment , size , pk , gasPrice, transactionFee) {

            var dialog = bootbox.dialog({
                    title   : $scope.pageTitle = $filter('translate')('Committing deposit request'),
                    message : '<p><i class="fa fa-spin fa-spinner"></i>Committing deposit request...</p>'
            });

            var req = {
                method: 'POST',
                url: UTIL_CONF.middle_server + UTIL_CONF.CONSTANT.DEPOSIT_URI,
                data:  {
                         userid: $scope.container.user.userid ,
                        detail : comment ,
                     assetname : assetname,
                       address : address,
                          size : String(size) ,
                          pk   : pk,
                      gasprice : gasPrice,
                      transactionFee : transactionFee
                    }
            }

            $http(req).then(
                (resp)=>{
                   if ( !resp.data.error ){
                        dialog.modal('hide');

                        if( assetname == UTIL_CONF.CONSTANT.BTC ){
                            showTransaction( UTIL_CONF.btcscanUrl , resp.data.tx );
                        }else{
                            showTransaction( UTIL_CONF.etherscanUrl , resp.data.tx );
                        }
                   }
            });

        }

        $scope.openDeposit = ()=> {
            fetchCurrentBalance();
            $("#depositModal").modal();
        }


        $scope.showTransactionModal = function () {

            var req = {
                method : 'POST',
                url    : UTIL_CONF.middle_server + UTIL_CONF.CONSTANT.TRANSACTION_URI,
                data   : {userid: $scope.container.user.userid}
            }

            $http(req).then(
                (resp)=>{
                   if (resp && resp.data ){
                        $scope.container.user.txs = resp.data;
                        if (!$scope.container.user.islogin) {
                            systemInfo("No wallet has been created yet", "operation failed");
                            return;
                        }
                        $("#exchangeModal").modal();
                   }
            });


        }

        $scope.showWithdrawModal = function () {
            if (!$scope.container.user.islogin) {
                systemInfo("No wallet has been created yet", "operation failed");
                return;
            }
            $("#withdrawModal").modal();
        }

        async function fetchCurrentBalance(){
            if (!$scope.container.user.islogin) {
                systemInfo("No wallet has been created yet", "operation failed");
                return;
            }

            var asset_balance_uri = String( $scope.container.params.asset ).toUpperCase() + '_BALANCE_URI';
            var money_balance_uri = String( $scope.container.params.money ).toUpperCase() + '_BALANCE_URI';

            var asset_req = {
                method: 'POST',
                url: UTIL_CONF.middle_server + UTIL_CONF.CONSTANT[asset_balance_uri],
                data:  { tokenName : String( $scope.container.params.asset ).toUpperCase() , address: $scope.container.user.wallet[$scope.container.params.assetAccount].address }
            }

            var money_req = {
                method: 'POST',
                url: UTIL_CONF.middle_server + UTIL_CONF.CONSTANT[money_balance_uri],
                data:  { tokenName : String( $scope.container.params.money ).toUpperCase() , address: $scope.container.user.wallet[$scope.container.params.moneyAccount].address }
            }

            var assetResp  =await $http(asset_req);
            var moneyResp  =await $http(money_req);

            $scope.container.user.wallet.balance[$scope.container.params.asset] = assetResp.data.balance;
            $scope.container.user.wallet.balance[$scope.container.params.money] = moneyResp.data.balance;
        }


        $scope.calculateBuy = function (e) {

            if ( !$scope.container.putOrder.input_amount_buy || !$scope.container.putOrder.input_price_buy) {
                $scope.container.putOrder.yourpaybuy = "";
                return;
            }

            var params = $scope.container.params;
            var asset = params.marketDecimal[params.market][params.asset];
            if (asset === 0) {
                $scope.container.putOrder.input_amount_buy =  $scope.container.putOrder.input_amount_buy.replace('.','')
            }

            var input_amount = new BigDecimal($scope.container.putOrder.input_amount_buy);
            var input_price  = new BigDecimal($scope.container.putOrder.input_price_buy);
            var result       = input_amount.multiply(input_price);
            if( result ){
                $('#buy-btn').prop("disabled", false);
                $scope.container.putOrder.yourpaybuy = result.toString();
            }
        }


        $scope.calculateSell = function (keyEvent) {
            if ( !$scope.container.putOrder.input_amount_sell || !$scope.container.putOrder.input_price_sell ){
                return  $scope.container.putOrder.yourpaysell = "";
            }

            var params = $scope.container.params;
            var asset = params.marketDecimal[params.market][params.asset];
            if (asset === 0) {
                $scope.container.putOrder.input_amount_sell =  $scope.container.putOrder.input_amount_sell.replace('.','')
            }

            var input_amount = new BigDecimal($scope.container.putOrder.input_amount_sell);
            var input_price  = new BigDecimal($scope.container.putOrder.input_price_sell);
            var result       = input_amount.multiply(input_price);
            if( result ){
                $('#sell-btn').prop("disabled", false);
                $scope.container.putOrder.yourpaysell = result.toString();
            }
        }



        $scope.buy = function () {

            if ( !$scope.container.putOrder.input_amount_buy || $scope.container.putOrder.input_amount_buy <= 0) {
                systemInfo("Please input effective amount", "operation failed");
                return;
            }
            if ( ! $scope.container.putOrder.input_price_buy || $scope.container.putOrder.input_price_buy <= 0) {
                systemInfo("Please input effective price", "operation failed");
                return;
            }

            if ( ! $scope.container.user.islogin ) {
                    bootbox.confirm({
                        className: "demandWallet",
                        title: $scope.pageTitle = $filter('translate')("You are not logged in"),
                        message: 'Please <a href="#signupModal" data-toggle="modal" data-dismiss="modal">Create wallet</a> or' +
                            ' <a href="#loginModal" data-toggle="modal" data-dismiss="modal">Import wallet</a>',
                        callback: function (result) {}
                    });
            }else{
                    bootbox.confirm({
                            title: "Confirm",
                            message: 'Do you want to submit BUY transaction?',
                            callback: function (result) { if(result){buy()}}
                    });
            }
        };

        function buy() {
            var req = {
                method: 'POST',
                   url: UTIL_CONF.middle_server + UTIL_CONF.CONSTANT.TRADE_PUTLIMIT_URI,
                  data: {
                         userid : $scope.container.user.userid ,
                         market : $scope.container.params.market ,
                           side : 2 ,
                         amount : $scope.container.putOrder.input_amount_buy,
                         price  : $scope.container.putOrder.input_price_buy,
                         frozen : $scope.container.params.money
                        }
            }

            $http(req).then((resp)=>{
                if ( !resp.data.id ) {
                    systemInfo(data.error.message, "operation failed");
                    return;
                }
                $scope.container.putOrder.input_amount_buy = null;
                $scope.container.putOrder.input_price_buy  = null;
                $scope.container.putOrder.yourpaybuy       = null;
            },(err)=>{
                console.log(err);
            });
        }


        $scope.sell = function () {

            if (!$scope.container.putOrder.input_amount_sell || $scope.container.putOrder.input_amount_sell <= 0) {
                systemInfo("Please input effective amount", "operation failed");
                return;
            }
            if (!$scope.container.putOrder.input_price_sell || $scope.container.putOrder.input_price_sell <= 0) {
                systemInfo("Please input effective price", "operation failed");
                return;
            }

            if ( ! $scope.container.user.islogin ) {
                bootbox.confirm({
                    className: "demandWallet",
                    title: $scope.pageTitle = $filter('translate')("You are not logged in"),
                    message: 'Please <a href="#signupModal" data-toggle="modal" data-dismiss="modal">Create wallet</a> or' +
                        ' <a href="#loginModal" data-toggle="modal" data-dismiss="modal">Import wallet</a>',
                    callback: function (result) {}
                });
            }else{
                bootbox.confirm({
                        title: "Confirm",
                        message: 'Do you want to submit SELL transaction?',
                        callback: function (result) {
                                                        if(result){
                                                            sell();
                                                        }
                                                    }
                                });

            }
        }

        function sell(){
            var req = {
                method: 'POST',
                url   : UTIL_CONF.middle_server + UTIL_CONF.CONSTANT.TRADE_PUTLIMIT_URI,
               data   : { userid:$scope.container.user.userid ,market: $scope.container.params.market , side: 1 , amount : $scope.container.putOrder.input_amount_sell,price:$scope.container.putOrder.input_price_sell,frozen:$scope.container.params.asset}
            }
            $http(req).then(
            (resp)=>{
                    if ( !resp.data.id ) {
                        systemInfo(data.error.message, "operation failed");
                        return;
                    }

                $scope.container.putOrder.input_amount_sell = null;
                $scope.container.putOrder.input_price_sell = null;
                $scope.container.putOrder.yourpaysell = null;
            },(err)=>{
                console.log(err);
            });

        }



$scope.setSellValue=function( data ){
        var input_yourpaysell = new BigDecimal('0');

        if( !$scope.container.user.traderBalance){
            input_yourpaysell = new BigDecimal( $scope.container.user.traderBalance[$scope.container.params.asset].available.toString());
        }
        var input_price = new BigDecimal(data.price.toString());
        $scope.container.putOrder.input_price_sell  = data.price;
        $scope.container.putOrder.yourpaysell       = input_yourpaysell.toString();
        $scope.container.putOrder.input_amount_sell = input_yourpaysell.multiply(input_price).toString();
}


$scope.setBuyValue = function ( data ) {
        var input_yourpaybuy = new BigDecimal('0');
        if( !$scope.container.balance){
          input_yourpaybuy = new BigDecimal($scope.container.user.traderBalance[$scope.container.params.money].available.toString());
        }
        var input_price = new BigDecimal(data.price.toString());
        $scope.container.putOrder.input_price_buy  = input_price.toString();
        $scope.container.putOrder.yourpaybuy       = input_yourpaybuy.toString();
        $scope.container.putOrder.input_amount_buy = input_yourpaybuy.divide(input_price);
}

$scope.cancel = function ( orderid ){
            bootbox.confirm($scope.pageTitle = $filter('translate')("Are you sure to cancel this order?"),
                function (result) {
                    if (result) {
                        cancel( orderid );
                    }
                });
            }
        function cancel( orderid ) {

            var req = {
                method: 'POST',
                url: UTIL_CONF.middle_server + UTIL_CONF.CONSTANT.TRADE_CANCEL_URI,
                data:  { userid : $scope.container.user.userid , market: $scope.container.params.market ,orderid : parseInt(orderid), money : $scope.container.params.money }
            }

            $http(req).then(
                (resp)=>{
                   if (resp.data.error) {
                        systemInfo(data.error.message, "Cancel error");
                        return;
                    }

                    if (resp.data) {
                        systemInfo("Cancel successfully");
                        angular.forEach($scope.container.user.openOrders, function (value, key) {
                            if (value.orderid == resp.data.id) {
                                value.Cancel = true;
                            }
                        }, null);
                    }
            });
        }

        function systemInfo(msg, title) {
            var bb = bootbox.confirm({
                title: $scope.pageTitle = $filter('translate')(title),
                message: $scope.pageTitle = $filter('translate')(msg),
                buttons: {},
                callback: function () {}
            });

            bb.bind('shown.bs.modal', function () {
                $('.modal-dialog').each(function (index, elem) {
                    $(elem).draggable({
                        handle: ".modal-header"
                    });
                });
            });
        }
        $scope.pairChange = function () {
            var selMarket = $scope.container.params.sel_market;
            var sel_market = selMarket.split('-');
            var mPara = '?m=' + sel_market[0]+'-'+sel_market[1]+ '&c=' + sel_market[0];
            $window.location.href = $window.location.origin + '/main.html' + mPara;
        }

        $scope.showHome = function() {
            $window.location.href = 'index.html';
        }
        function handleUserData()
        {
            $scope.fetchUserData($scope.container.user.userid);
        }

        function initParam()
        {
            if( $scope.container.user.wallet ){
                $scope.container.user.wallet.deposit  = {};
                $scope.container.user.wallet.deposit[$scope.container.params.asset] = 0;
                $scope.container.user.wallet.deposit[$scope.container.params.money] = 0;
                $scope.container.user.wallet.withdraw = {};
                $scope.container.user.wallet.withdraw[$scope.container.params.asset] = 0;
                $scope.container.user.wallet.withdraw[$scope.container.params.money] = 0;
            }
            if( $scope.container.user.confirmedBlock ){
                $scope.container.user.confirmedBlock[$scope.container.params.asset] = 0;
                $scope.container.user.confirmedBlock[$scope.container.params.money] = 0;
            }
            if( $scope.container.user.confirmBlockNumber ){
                $scope.container.user.confirmBlockNumber[$scope.container.params.asset] = 0;
                $scope.container.user.confirmBlockNumber[$scope.container.params.money] = 0;
            }
            if( $scope.container.params ){
                $scope.container.params.marketDecimal = UTIL_CONF.marketDecimal;
                $scope.container.params.scanner       = UTIL_CONF.scanner;
            }
        }


    }
]);

