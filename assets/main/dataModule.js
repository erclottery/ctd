var dataModule = angular.module('dataModule', ['ngResource', 'ngSessionStorage', 'ngStorage'],
 ['$locationProvider',function ($locationProvider) {$locationProvider.html5Mode({enabled: true,requireBase: false});}]);

dataModule.controller('dataController', [
    '$scope',
    '$location',
    '$http',
    'UTIL_CONF',
    function (
        $scope,
        $location,
        $http,
        UTIL_CONF
    ) {
        $scope.initAfterClearWallet = function () {
            setParams();
        }

        setParams();

        function setParams() {
            io.sails.url = UTIL_CONF.datapush_server;
            var pairs = $location.search()["m"];
            var pairsdata = '';
            if(pairs != null) {
                pairsdata = pairs.split('-');
                if(pairsdata.length < 1) {
                    return false;
                }
            }
            $scope.container.params.sel_market   = pairs;
            $scope.container.params.market       = pairsdata[0] + pairsdata[1];
            $scope.container.params.money        = pairsdata[1];
            $scope.container.params.moneyAccount = pairsdata[1]+'Account';
            $scope.container.params.asset        = pairsdata[0];
            $scope.container.params.assetAccount = pairsdata[0]+'Account';
            $scope.container.params.start        = GetDateStr(-360);
            $scope.container.params.end          = new Date().getTime();
            $scope.container.params.interval     = "86400";
            $scope.container.params.type         = "kline";


            var req = { method: 'GET',url: UTIL_CONF.middle_server + UTIL_CONF.CONSTANT.CONFIG_PAIRS_URI}

            $http(req).then((resp)=>{
                $scope.container.params.pairs = resp.data.pairs;
            }, (err)=>{
                console.log(err);
            });

            fetchMarketData();
        }

        $scope.fetchMarketData = function () {
            var req =   {
                            method : 'POST',
                            url    : UTIL_CONF.datapush_server + UTIL_CONF.CONSTANT.MARKET_URI,
                            data   : {
                                        market   : $scope.container.params.market ,
                                        start    : $scope.container.params.start,
                                        end      : $scope.container.params.end,
                                        interval : $scope.container.params.interval
                                    }
                        }

            $http(req).then((resp)=>{
                $scope.$broadcast('event:loadFinish', 'addfinish');
                if( resp && resp.data ){
                    $scope.container.market.market = resp.data.market;
                }  
            }, (err)=>{
                console.log(err);
            });
        }

        function fetchMarketData()
        {
            if($scope.container.params.marketEvent){
                io.socket.off($scope.container.params.marketEvent, ()=>{
                    console.log("unbind the event",$scope.container.params.marketEvent);
                });
            }

             var param =$scope.container.params.market;
             $scope.container.params.marketEvent = param;

             io.socket.get( UTIL_CONF.datapush_server + UTIL_CONF.CONSTANT.MARKET_SUBSCRIBE_URI+param,(data, jwRes) =>{
               getMarketData();
               io.socket.on(param,  (data)=> {
                if(data){
                    setMarketData(data,"add");
                }

               });
            });
        }

        function getMarketData(flag) {

            var req =   {
                            method : 'POST',
                            url    : UTIL_CONF.datapush_server + UTIL_CONF.CONSTANT.MARKET_URI,
                            data   : {
                                    market   : $scope.container.params.market ,
                                    start    : $scope.container.params.start,
                                    end      : $scope.container.params.end,
                                    interval : $scope.container.params.interval
                                    }
                        }

            $http(req).then((resp)=>{
                setMarketData( resp.data );
            }, (err)=>{
                console.log(err);
            });

        }

        function setMarketData(data,flag){

                 if (flag != 'add'){
                    data.orderBookBuy.orders = combineOrders(data.orderBookBuy.orders);
                    data.orderBookBuy.total  = data.orderBookBuy.orders.length;

                    data.orderBookSell.orders = combineOrders(data.orderBookSell.orders);
                    data.orderBookSell.total  = data.orderBookSell.orders.length;
                 }

                if (data.markethistory && !angular.equals($scope.container.market.markethistory, data.markethistory)) {
                    if( flag !='add' ){
                        $scope.container.market.markethistory = data.markethistory;
                    }else{
                        $scope.container.market.markethistory.push( data.markethistory[1][0] );
                        $scope.container.market.markethistory = _.reverse( _.sortBy($scope.container.market.markethistory, [function(o) { return o.time; }]));
                    }


                }
                if ( data.orderBookBuy && (!angular.equals($scope.container.market.orderBookBuy, data.orderBookBuy))) {
                    if( flag !='add' || data.orderflag==true){
                        $scope.container.market.orderBookBuy = data.orderBookBuy;
                    }else{
                        processOrder("buy",data.orderBookBuy.orders[0]);
                    }

                    $scope.container.market.orderBookBuy.orders = _.sortBy($scope.container.market.orderBookBuy.orders, [function(o) { return o.price; }]);
                }
                if ( data.orderBookSell && (!angular.equals($scope.container.market.orderBookSell, data.orderBookSell))) {
                    if( flag !='add' || data.orderflag==true){
                        $scope.container.market.orderBookSell = data.orderBookSell;
                    }else{
                        processOrder("sell",data.orderBookSell.orders[0]);
                    }
                   $scope.container.market.orderBookSell.orders = _.sortBy($scope.container.market.orderBookSell.orders, [function(o) { return o.price; }]);
                }
                if ($scope.container.market && $scope.container.market.markethistory){
                    processTradeHistory($scope.container.market.markethistory);
                }

                if (data.market && !angular.equals($scope.container.market.market, data.market)){
                    $scope.container.market.market = data.market;
                }

                if ( data.market && flag == 'add' ){
                    $scope.$broadcast('event:loadFinish', 'addfinish');
                } else if( data.market ){
                    $scope.$broadcast('event:loadFinish', 'initfinish');
                }


                if ($scope.container.market.header && $scope.container.market.header.open <= '0') {
                    $scope.container.market.changeRate = "-";
                }

                if (data.header){
                    if(flag != 'add'){
                        $scope.container.market.header = data.header;
                        var last = parseFloat( $scope.container.market.header.last );
                        var open = parseFloat( $scope.container.market.header.open );
                        $scope.container.market.changeRate = (last - open) * 100 / open;
                    }else{
                        $scope.container.market.header = data.header[1];
                        var last = parseFloat( data.header[1].last );
                        var open = parseFloat( data.header[1].open );
                        $scope.container.market.changeRate = (last - open) * 100 / open;
                    }
                }




        }

        function processOrder(side,data){

            var orders = [];
            var orderExist = false;

            if( side =='buy' && data ){
                for (var i = $scope.container.market.orderBookBuy.orders.length - 1; i >= 0; i--) {

                   if( $scope.container.market.orderBookBuy.orders[i].price == data.price ){
                        if( data.left != 0 || data.left != '0'){
                            orders.push(data);
                        }
                        orderExist = true;
                   }else{
                        orders.push($scope.container.market.orderBookBuy.orders[i]);
                   }
                }

                if( !orderExist ){
                    orders.push(data);
                }
                $scope.container.market.orderBookBuy.orders = orders;
            }

            if( side =='sell' && data ){
                for (var i = $scope.container.market.orderBookSell.orders.length - 1; i >= 0; i--) {
                   if( $scope.container.market.orderBookSell.orders[i].price == data.price ){
                        if( data.left != 0 || data.left != '0'){
                            orders.push(data);
                        }
                        orderExist = true;
                   }else{
                        orders.push($scope.container.market.orderBookSell.orders[i]);
                   }
                }

                if( !orderExist ){
                    orders.push(data);
                }

                $scope.container.market.orderBookSell.orders = orders;
            }


        }

        function GetDateStr(AddDayCount) {
            var dd = new Date();
            dd.setDate(dd.getDate() + AddDayCount);
            return dd.getTime();
        }

        function processTradeHistory(data) {

            $scope.container.market.tradeHistoryShow = [];
            angular.forEach(data, function (value, key) {

                    var tmp = {};
                    tmp.Date        = (value.time + "").substring(0, 10) + (value.time + "").substring(11, 14);
                    tmp.Price       = value.price;
                    tmp.Amount      = value.amount;

                    if (value.type  == 'buy') {
                        tmp.BuyOrSell = true;
                    }else if (value.type  == 'sell') {
                        tmp.BuyOrSell = false;
                    }
                    $scope.container.market.tradeHistoryShow.push(tmp);


            }, null);
        }


         ////////////////////////
         //     USER DATA      //
         //                    //
         ////////////////////////

        $scope.fetchUserData = function ( userid ) {
            fetchUserData( userid );
        }

        function fetchUserData( userid ){
             var param = userid;
             io.socket.get( UTIL_CONF.datapush_server + UTIL_CONF.CONSTANT.MARKET_SUBSCRIBE_URI+param,(data, jwRes) =>{
                getUserData(userid);
                io.socket.on(param,  (data)=> {
                    if(data){
                        setUpdatedUserData(data);
                    }
               });
            });
        }

        function getUserData( userid ) {
            var req =   {
                            method : 'POST',
                            url    : UTIL_CONF.datapush_server + UTIL_CONF.CONSTANT.USER_URI,
                            data   : {
                                    market   : $scope.container.params.market ,
                                    userid   : userid
                                    }
                        }
            $http(req).then((resp)=>{
                setUserOrderData(resp.data);
            }, (err)=>{
                console.log(err);
            });

            var req =   {
                method : 'POST',
                url    : UTIL_CONF.middle_server + UTIL_CONF.CONSTANT.USER_BALANCE_URI,
                data   : {
                          userid   : userid
                        }
            }
            $http(req).then((resp)=>{
                setUserBalance(resp.data);
            }, (err)=>{
                console.log(err);
            });
        }
        function setUpdatedUserData( data){
            if(data && data.asset){
                setUserBalance(data.asset);
            }
            if(data && data.order){
                addOpenOrders(data.order)
            }

            if(data && data.finishedOrder){
                updateFinishedOrder(data.finishedOrder);
            }

            if(data && data.receiveObject){
               updateConfirmBlock(data);
            }

            if(data && data.assetHistoryOut){
               updateAssetOut(data);
            }

            if(data && data.assetHistoryIn){
               updateAssetIn(data);
            }
            $scope.$apply();
        }

        function updateAssetOut(data){
            if( !$scope.container.user.tx ){
                $scope.container.user.tx = {};
            }

            if( !$scope.container.user.tx[data.assetHistoryOut.asset]){
                $scope.container.user.tx[data.assetHistoryOut.asset] = {};
            }

            $scope.container.user.tx[data.assetHistoryOut.asset].withdraw = data.assetHistoryOut.detail;

            console.log('$scope.container.user.tx',$scope.container.user.tx);
        }

        function updateAssetIn(data){
            if( !$scope.container.user.tx ){
                $scope.container.user.tx = {};
            }

            if( !$scope.container.user.tx[data.assetHistoryIn.asset]){
                $scope.container.user.tx[data.assetHistoryIn.asset] = {};
            }

            $scope.container.user.tx[data.assetHistoryIn.asset].deposit = data.assetHistoryIn.detail;
        }

        function updateConfirmBlock( data ){
            $scope.container.user.confirmBlockNumber[data.receiveObject.assetname]   = data.receiveObject.confirmBlockNumber;
            $scope.container.user.confirmedBlock[data.receiveObject.assetname]       = data.receiveObject.confirmedBlock;
        }

        function updateFinishedOrder(finishedOrder){
            if( !finishedOrder.left || finishedOrder.left == '0e-8' ){
                updataMyOrder( finishedOrder );
                removeFromOpenOrders( finishedOrder.id );
            }else{
                updateOpenOrders( finishedOrder );
            }
        }

        function removeFromOpenOrders(orderid){
            _.remove($scope.container.user.openOrders,
                    function(order) {
                        return parseInt( order.orderid ) == parseInt(orderid);
                    });
        }

        function setUserBalance(data){
            if( data ){
                if( !$scope.container.user.traderBalance[$scope.container.params.asset] ){
                    $scope.container.user.traderBalance[$scope.container.params.asset] = {};
                }

                if( !$scope.container.user.traderBalance[$scope.container.params.money] ){
                    $scope.container.user.traderBalance[$scope.container.params.money] = {};
                }
                $scope.container.user.traderBalance[$scope.container.params.asset].available = data[String($scope.container.params.asset).toLowerCase()+'Available'];
                $scope.container.user.traderBalance[$scope.container.params.money].available = data[String($scope.container.params.money).toLowerCase()+'Available'];
                $scope.container.user.traderBalance[$scope.container.params.asset].frozen    = data[String($scope.container.params.asset).toLowerCase()+'Frozen'];
                $scope.container.user.traderBalance[$scope.container.params.money].frozen    = data[String($scope.container.params.money).toLowerCase()+'Frozen'];

            }
        }

        function setUserOrderData(data) {

                if ( $scope.container && data.container.openOrders && data.container.openOrders.records ) {
                    processOpenOrders( data.container.openOrders.records );
                }

                if ( $scope.container && data.container.finishedOrder && data.container.finishedOrder.records ) {
                    processMyOrder( data.container.finishedOrder.records );
                }
        }

        function updataMyOrder(value){
            var tmp = {};
                tmp            = value;
                tmp.time       = (value.ctime + "").substring(0, 10) + (value.ctime + "").substring(11, 14);
                tmp.deal_money = tmp.deal_money;
                tmp.price      = tmp.price;
            $scope.container.user.finishedOrder.push(tmp);
        }


        function processMyOrder(data) {
            $scope.container.user.finishedOrder = [];
            angular.forEach(data, function (value, key) {
                var tmp = {};
                tmp            = value;
                tmp.time       = (value.ctime + "").substring(0, 10) + (value.ctime + "").substring(11, 14);
                tmp.deal_money = tmp.deal_money;
                tmp.price      = tmp.price;
                $scope.container.user.finishedOrder.push(tmp);
            }, null);
        }

        function updateOpenOrders(value){
           var tmporder = _.find($scope.container.user.openOrders,
            function(o) {
                return o.orderid == value.id;
            });
           tmporder.FilledAmount = parseFloat(value.deal_stock);
        }

        function addOpenOrders(value){
               $scope.container.user.openOrders.push({
                    //Date: (value.ctime + "").substring(0, 10) + (value.ctime + "").substring(11, 14),
                    Date: (Math.trunc(value.ctime) + "").substring(0, 10) + (Math.trunc(value.ctime) + "").substring(11, 14),
                    Type: parseInt(value.side),
                    Price: parseFloat(value.price),
                    PriceUp: false,
                    Amount: parseFloat(value.amount),
                    FilledAmount:parseFloat(value.dealstock),
                    Total: (new BigDecimal(value.price)).multiply(new BigDecimal(value.amount)).toString(),
                    Cancel: false,
                    orderid: value.orderid
                });
               $scope.container.user.openOrdersCnt = $scope.container.user.openOrders.length;

        }

        function processOpenOrders(data) {
            if ( !$scope.container.user.openOrders || $scope.container.user.openOrders.length > 0) {
                $scope.container.user.openOrders = [];
            }

            angular.forEach(data, function (value, key) {

                $scope.container.user.openOrders.push({
                    //Date: (value.ctime + "").substring(0, 10) + (value.ctime + "").substring(11, 14),
                    Date: (Math.trunc(value.ctime) + "").substring(0, 10) + (Math.trunc(value.ctime) + "").substring(11, 14),
                    Type: parseInt(value.side),
                    Price: parseFloat(value.price),
                    PriceUp: false,
                    Amount: parseFloat(value.amount),
                    FilledAmount:parseFloat(value.deal_stock),
                    Total: (new BigDecimal(value.price)).multiply(new BigDecimal(value.amount)).toString(),
                    Cancel: false,
                    orderid: value.id
                });
            });
            $scope.container.user.openOrdersCnt = $scope.container.user.openOrders.length;
        }

        /**
         * Combine similar orders
         * @param orderArray
         */
        function combineOrders(orderArray) {
            var aResults = [];
            var n = 0, j = 0;

            angular.forEach(orderArray, function(item, i) {
                for(j = 0; j < n; j++) {
                    if(aResults[j].price == item.price) {
                        aResults[j].amount = parseFloat(item.amount) + parseFloat(aResults[j].amount);
                        aResults[j].left = parseFloat(item.left) + parseFloat(aResults[j].left);
                        break;
                    }
                }
                if(j == n) {
                    aResults.push(item);
                    n++;
                }
            });

            return aResults;
        }
    }
]);
