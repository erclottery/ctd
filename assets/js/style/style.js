// change theme
//<!--<link href="../styles/theme_white/index.css" rel="stylesheet">-->
// add function
function addWhiteTheme() {
    // loadjscssfile('../styles/theme_white/index.css', 'css', "theme-css-white");
    // loadjscssfile('../styles/theme_white/login.css', 'css', "theme-css-white-login");
    // removejscssfile('../../bower_components/highcharts/themes/dark-unica.src.js', 'js');
}

// remove function
function removeWhiteTheme() {
    // removejscssfile('../styles/theme_white/index.css', 'css', "theme-css-white");
    // removejscssfile('../styles/theme_white/login.css', 'css', "theme-css-white-login");
    // loadjscssfile('../../bower_components/highcharts/themes/dark-unica.src.js', 'js', "highcharts-dark");
}

// change function
function useWhiteTheme(useWhite) {
    // Cookies.set('nutzam-theme', useWhite ? "dark" : "white");
    // if (useWhite) {
    //     addWhiteTheme();
    // } else {
    //     removeWhiteTheme();
    // }
}
//jQuery(document).ready(function() {
    // getThemeCSSName();
    // jQuery("#changeTheme").click(function() {
    //     useWhiteTheme(getThemeCSSName() == 'white');
    // });
//});

// 获取cookie中选中的主题名称，没有就给个默认的
function getThemeCSSName() {
    // return Cookies.get('nutzam-theme') || "white";
}



// auto set ele height 
//window.onload = function() {
    var winH 
//$(document).ready(function() {
window.onload = function() {
   winH = document.body.clientHeight || document.documentElement.clientHeight;
    calcEleHeight();
    $("#toMarketHistory").click(function(){ 
        calcEleHeight(); 
    }); 
    $("#toMyHistory").click(function(){ 
        calcEleHeight(); 
    }); 
};
//});

function calcEleHeight(){
    // var items = byClass("table-container");
    // items.forEach(function(item, index) {
    //     var height = getCss(item, "height");
    //     if (height > (winH - 447)) {
    //         item.style.height = (winH - 447) + "px";
    //     }
    // });
    var boxes = byClass("table-box");
    boxes.forEach(function(item, index) {
        var height = getCss(item, "height");
        item.style.height = (winH - 447) + "px";
    });
}

function byClass(strClass, context) {
    context = context || document;
    var ary = [];
    var tagList = context.getElementsByTagName("*");
    strClass = strClass.replace(/(^\s+)|(\s+$)/g, '').split(/\s+/g);
    for (var i = 0; i < tagList.length; i++) {
        var curTag = tagList[i],
            curTagClass = curTag.className;
        //排除法：不管当前标签是否符合，我们先放在ary容器中，然后再循环strClass，把不符合的在数组中排除掉即可
        ary.push(curTag);
        for (var j = 0; j < strClass.length; j++) {
            var reg = new RegExp('(^| +)' + strClass[j] + '( +|$)');
            if (!reg.test(curTagClass)) {
                ary.length--;
                break;
            }
        }
    }
    return ary;
}
//->getCss：gets the style property value of the element
function getCss(curEle, attr) {
    var val = null;
    var isHighVersion = 'getComputedStyle' in window;
    if (isHighVersion) {
        val = window.getComputedStyle(curEle, null)[attr];
    } else {
        if (attr.toLowerCase() === 'opacity') {
            val = curEle.currentStyle['filter'];
            reg = /^alpha\(opacity=(.+)\)$/i;
            val = reg.test(val) ? reg.exec(val)[1] / 100 : 1;
        } else {
            val = curEle.currentStyle[attr];
        }
    }
    var temp = parseFloat(val);
    val = isNaN(temp) ? val : temp;
    return val;
}