var transEngine = null;

var homeModule = angular.module('homeModule', [
    'ngResource', 'ngStorage' , 'exchange.conf', 'pascalprecht.translate'
]);
homeModule.controller('homeController', [
    '$scope', '$http','UTIL_CONF','$window','$localStorage','$filter',
    function (
        $scope, $http, UTIL_CONF, $window, $localStorage ,$filter
    ) {
        function init() {
            setParam();
            connectSocketIO();
            loadWallet();
        }

        function loadWallet(){
            if( $localStorage.user && $localStorage.user.wallet )
            {
                $scope.container.user               = $localStorage.user;
                $scope.container.user.Authorization = "Bearer " + $localStorage.user.token;
                $scope.container.user.islogin       = true;
                $http.defaults.headers.common['Authorization'] = $scope.container.user.Authorization;
                loadAssetList();
            }
            else{
                $scope.container.user.islogin = false;
            }
        }

         function Balance(asset,address){
          return new Promise((resolve,reject)=>{
            var asset_balance_uri = String( asset ).toUpperCase() + '_BALANCE_URI';
            var asset_req = {
                method: 'POST',
                url: UTIL_CONF.middle_server + UTIL_CONF.CONSTANT[asset_balance_uri],
                data:  { tokenName : String( asset ).toUpperCase() , address: address }
            }
            $http(asset_req).then((resp)=>{
                resolve( resp.data.balance );
            });
          })

       }

        function loadAssetList()
        {
            $scope.container.user.assetList = [];
            var eth     = $scope.container.user.wallet[UTIL_CONF.CONSTANT.ETH+'Account'];
            eth.name    = 'Ethereum';
            eth.balance = 0;
            eth.price   = 0;
            eth.img     = '../home/images/image_eth.png';
            $scope.container.user.assetList.push(eth);



            var btc = $scope.container.user.wallet[UTIL_CONF.CONSTANT.BTC+'Account'];
            btc.name    = 'Bitcoin';
            btc.price   = 0;
            btc.balance = 0;
            btc.coin    = UTIL_CONF.CONSTANT.BTC;
            btc.img     = '../home/images/image_btc.png';
            $scope.container.user.assetList.push(btc);

            var elot     = $scope.container.user.wallet[UTIL_CONF.CONSTANT.ELOT+'Account'];
            elot.name    = 'Elot lottery';
            elot.coin    = UTIL_CONF.CONSTANT.ELOT;
            elot.balance = 0;
            elot.price   = 0;
            elot.img     = '../home/images/img_elot.png';
            $scope.container.user.assetList.push(elot);


            var eos     = $scope.container.user.wallet[UTIL_CONF.CONSTANT.EOS+'Account'];
            eos.name    = 'EOS';
            eos.coin    = UTIL_CONF.CONSTANT.EOS;
            eos.balance = 0;
            eos.price   = 0;
            eos.img     = '../home/images/image_eos.png';
            $scope.container.user.assetList.push(eos);

        }

        $scope.fetchPrice = function(){

            var asset_req = {
                method: 'GET',
                url: UTIL_CONF.middle_server + UTIL_CONF.CONSTANT.PRICE_TRICKER_URI,
            }

            $http(asset_req).then((resp)=>{
               // console.log('###########resp######',resp.data);
               var keys = [];
               for(key in resp.data.data){
                    keys.push(key);
               }

              // console.log($scope.container.user.wallet);

               for (var i = keys.length - 1; i >= 0; i--) {
                   var key    = keys[i];
                   var symbol = resp.data.data[key].symbol;
                   // console.log(symbol);
                   // console.log(symbol+'Account');
                   if( $scope.container.user.wallet[symbol+'Account'] ){
                        var price = trimDecimal(resp.data.data[key].quotes.USD.price);
                        $scope.container.user.wallet[symbol+'Account'].price = price;
                        fetchEstimatedValue(symbol+'Account');
                   }
               }

            });


        }

        function fetchEstimatedValue( assetAccount){
                let price,balance;
                if( $scope.container.user.wallet[assetAccount] ){
                    price   = $scope.container.user.wallet[assetAccount].price;
                    balance = $scope.container.user.wallet[assetAccount].balance;
                }

                if(  price > 0 && balance > 0  ){
                    console.log(assetAccount ,balance,'balance');
                    console.log(assetAccount ,price,'price');

                   $scope.container.param.estimatedValue = trimDecimal($scope.container.param.estimatedValue +  price*balance);
                   console.log(assetAccount);
                }

        }

        $scope.fetchBalance = function(asset){

            if(asset.coin == UTIL_CONF.CONSTANT.ELOT ){
                 Balance( UTIL_CONF.CONSTANT.ELOT , asset.address ).then((balance)=>{

                    asset.balance = trimDecimal(balance);
                    fetchEstimatedValue(asset.coin+'Account');


                })
            }else if(asset.coin == UTIL_CONF.CONSTANT.EOS ){
                Balance( UTIL_CONF.CONSTANT.EOS , asset.address ).then((balance)=>{
                    asset.balance =  trimDecimal(balance) ;
                    fetchEstimatedValue(asset.coin+'Account');

                })
            }else if(asset.coin == UTIL_CONF.CONSTANT.BTC ){
                Balance( UTIL_CONF.CONSTANT.BTC , asset.address ).then((balance)=>{
                    asset.balance =  trimDecimal(balance) ;
                    fetchEstimatedValue(asset.coin+'Account');

                })
            }else if(asset.coin == UTIL_CONF.CONSTANT.ETH ){
                Balance( UTIL_CONF.CONSTANT.ETH , asset.address ).then((balance)=>{
                    asset.balance =  trimDecimal(balance) ;
                    fetchEstimatedValue(asset.coin+'Account');

                })
            }
        }

        function trimDecimal(decimal){
            return Math.round(decimal*1000)/1000;
        }


        function connectSocketIO(){
            io.sails.url = UTIL_CONF.datapush_server;
            var socket_test = io.sails.connect();
            socket_test.get(UTIL_CONF.mainpage_data_url, (data, jwRes) =>{
                //第一次加载，所有的数据
                updateMarkets(data);
                socket_test.on(UTIL_CONF.mainpage_event,  (data) => {
                    updateMarkets(data);
                });
            });
        }

        function setParam(){
            if( !$scope.container ){
                $scope.container = {};
            }

            if( !$scope.container.param ){
                $scope.container.param  = {};
            }

            $scope.container.param.seachFitter = '';
            $scope.container.param.currentPage = 'marketList';

            if( !$scope.container.market ){
                $scope.container.market  = {};
            }

            if( !$scope.container.user ){
                $scope.container.user  = {};
            }

            $scope.container.param.estimatedValue =0;

            // 设定语言列表
            $scope.languages = [{ "value": 'en', "name": "English" }, { "value": 'ja', "name": "日本語" }, { "value": 'ko', "name": "한국어<" }, { "value": 'zh_CN', "name": "简体中文" }];
            $scope.c_lang    = window.localStorage.lang || 'en';
            $scope.switch_language = function (lang) {
                window.localStorage.lang = lang;
                $scope.c_lang = lang;
                transEngine.preferredLanguage(lang);
                window.location.reload(false);
            };
        }

        function updateMarkets(data) {

            for(var i = 0; i < data.length; i++) {
                var openPrice  = parseFloat(data[i].result.open);
                var closePrice = parseFloat(data[i].result.last);
                data[i].result.swing = (closePrice - openPrice) * 100 / openPrice;
            }

            // filter the data by the tab
            $scope.container.market.marketStatus = data;
            $scope.$apply();
        }

        init();

        $scope.changeCurrentPage = function(currentPage){
        	$scope.container.param.currentPage = currentPage;
        }


        $scope.redirectExchange = function(data){
            var params = String( data.market ).split('/');
            var mPara = '?m=' + params[0]+'-'+params[1]+ '&c=' + params[0];
            $window.location.href = $window.location.origin + '/main.html' + mPara;
        }

        $scope.transfer = function(){
            if( $scope.container.param.currentAsset.coin == UTIL_CONF.CONSTANT.ETH){
                transferETH();
            } else if ( $scope.container.param.currentAsset.coin == UTIL_CONF.CONSTANT.BTC){
                transferBTC();
            } else {
                transferToken();
            }

        }

        function transferToken(){
            var transfer_req = {
                method: 'POST',
                url: UTIL_CONF.middle_server + UTIL_CONF.CONSTANT.TOKEN_TRANSFER_URI,
                data:  {
                          to       : String( $scope.container.param.toAddress ) ,
                          pk       : $scope.container.user.wallet.ETHAccount.privateKey ,
                          gasprice : $scope.container.user.wallet.ethGasprice,
                          amount   : $scope.container.param.transferSize,
                          tokenName: $scope.container.param.currentAsset.coin
                      }
            }

            $http(transfer_req).then((resp)=>{
              showTransaction( UTIL_CONF.etherscanUrl , resp.data.txid );
            });

        }

        function transferBTC(){
            var transfer_req = {
                method: 'POST',
                url: UTIL_CONF.middle_server + UTIL_CONF.CONSTANT.BTC_TRANSFER_URI,
                data:  {
                    toaddress      : String( $scope.container.param.toAddress ) ,
                          pk       : $scope.container.user.wallet.BTCAccount.privateKey ,
                    transactionfee : $scope.container.user.wallet.btcFee,
                    amounttosend   : $scope.container.param.transferSize
                      }
            }

            $http(transfer_req).then((resp)=>{
                if( resp && resp.data && resp.data.hash ){
                    showTransaction( UTIL_CONF.btcscanUrl , resp.data.hash );
                }     
            });
       }

        function transferETH(){
              var transfer_req = {
                  method: 'POST',
                  url: UTIL_CONF.middle_server + UTIL_CONF.CONSTANT.ETH_TRANSFER_URI,
                  data:  {
                            to       : String( $scope.container.param.toAddress ) ,
                            pk       : $scope.container.user.wallet.ETHAccount.privateKey ,
                            gasprice : $scope.container.user.wallet.ethGasprice,
                            amount   : $scope.container.param.transferSize
                        }
              }

              $http(transfer_req).then((resp)=>{
                showTransaction( UTIL_CONF.etherscanUrl , resp.data.txid );
              });
         }

         function showTransaction( url ,tx ) {
            bootbox.alert({
                title: $scope.pageTitle = $filter('translate')("Deposit success"),
                message: "<h1>You just created transactions. Track their progress:</h1>" +
                "<a style=\"font-size:85%\" target=_blank href=" + url + tx + ">" + tx + "</a>",
                callback: function (result) {}
            });
        }

        $scope.showQRCode = function(ev,address){
            var qrcode_req = {
                method: 'POST',
                url: UTIL_CONF.middle_server + UTIL_CONF.CONSTANT.QRCODE_URI,
                data:  { address : String( address )}
            }

            $http(qrcode_req).then((resp)=>{
              bootbox.dialog({
                title: $scope.pageTitle = $filter('translate')(" <h1>"+address
                    +"</h1>"),
                message: "<img class='qrcode-dialog' src='"+resp.data.img+"'/>",
                buttons: {

                        noclose: {
                            label: "copy",
                            className: 'btn-primary',
                            callback: function(){

                                copyAddress(ev,address);
                                return false;
                            }
                        },
                        cancel: {
                            label: "close",
                            className: 'btn-danger',
                            callback: function(){}
                        }
                    }
                });
            });
        }

        function copyAddress (ev,address){

            if(address){
                var obj = ev.path[1]
                var range = document.createRange();
                range.selectNode(obj);
                window.getSelection().removeAllRanges();
                window.getSelection().addRange(range);
                var successful = document.execCommand('copy');
                if (successful) {
                    bootbox.alert("Copy Success")
                }else{
                    bootbox.alert(" Copy failed ")
                }
            }else{
                bootbox.alert(" Copy failed ")
            }

        }

        $scope.openTransfer = function(asset){
            $scope.container.param.currentAsset        = asset;
            //$scope.container.param.currentAssetBalance = asset.balance;
            $scope.container.param.toAddress = asset.address;

            $("#transferModal").modal();
        }



        $scope.queryFilter = function(marketInfo){
            var inputValue = $scope.container.param.seachFitter;
            if(inputValue === ""){
                return true;
            }else{
                var fitterIndex = marketInfo.market.toUpperCase().indexOf(inputValue.toUpperCase());
                var fitterCoinIndex = marketInfo.coin.toUpperCase().indexOf(inputValue.toUpperCase());
                if(fitterIndex < 0 && fitterCoinIndex < 0){
                    return false;
                }else{
                    return true;
                }
            }
        }


        $scope.clearStorage = function () {
            if (!$scope.container.user.islogin) {
                systemInfo("No wallet has been created yet", "operation failed");
                return;
            }
            bootbox.confirm({
                message: $scope.pageTitle = $filter('translate')("Please confirm the wallet key store has been BACKUP. KeyStore will not be restored by Exchange."),
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-danger'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-primary'
                    }
                },
                callback: function (result) {
                    if (result) {
                        clearStorage();
                        $window.location.reload();
                    } else {
                        return;
                    }
                }
            });
        }


        function clearStorage() {
            $http.defaults.headers.common['Authorization'] = null;
            $localStorage.$reset();
            $scope.container.user = null;
        }


        $scope.marketFavorite = function($event){
            $event.stopPropagation();
        }

        $scope.marketFavoriteEx = function($event , marketInfo){
            // console.log($event.srcElement.checked);
            if($localStorage.favoriteArr === undefined){
                $localStorage.favoriteArr = [];
            }

            var index = $localStorage.favoriteArr.indexOf(marketInfo.market);
            if(index < 0){
                if($event.srcElement.checked){
                    $localStorage.favoriteArr.push(marketInfo.market);
                }
            }else{
                if(!$event.srcElement.checked){
                    $localStorage.favoriteArr.splice(index, 1);
                }
            }
        }


        $scope.favoriteCheck = function(marketInfo){
            if($localStorage.favoriteArr === undefined){
                return false;
            }else{
                var index = $localStorage.favoriteArr.indexOf(marketInfo.market);
                if(index < 0){
                    return false;
                }else{
                    return true;
                }
            }
        }

        $scope.showSignupModal = function () {

            if ($scope.container.user.islogin) {
                systemInfo("One wallet has existed，pleas clear wallet", "operation failed");
                return;
            }

            $scope.container.user.password = '';
            $scope.container.user.repeatPassword = '';
            $("#signupModal").modal();
            getverifyCode()
        }

        $scope.newAccount = function () {

            var req = {
                method: "POST",
                url: UTIL_CONF.middle_server + UTIL_CONF.CONSTANT.CAPTCHAPNG_URI + UTIL_CONF.CONSTANT.VERIFYIMG_URI,
                data: {
                        id: $scope.container.captchapngId,
                        code: document.getElementById("verify-input2").value }
                    };
            $http(req).then(response => {
              if (response.data.error) {
                return systemInfo("Verifycode does not matched", "operation failed");
              }
              newAccount();
            });
        }

       function newAccount () {
            // input check
            if ( ! $scope.container.user.password ) {
                systemInfo("Please enter your wallet password", "operation failed");
                return;
            }
            if ( $scope.container.user.password.length < 8 ) {
                systemInfo("Passwords must be at least 8 characters", "operation failed");
                return;
            }
            if ( $scope.container.user.password != $scope.container.user.repeatPassword) {
                systemInfo("Password does not equals repeated password", "operation failed");
                return;
            }

            var dialog = bootbox.dialog({
                title: $scope.pageTitle = $filter('translate')("Creating wallet"),
                message: "<p><i class=\"fa fa-spin fa-spinner\"></i>" +
                    $filter('translate')("Creating wallet") +
                    "...</p>",
            });

            var req = {
                method: 'POST',
                url: UTIL_CONF.middle_server + UTIL_CONF.CONSTANT.CREAT_WALLET_URI,
                data:  { password : $scope.container.user.password , repeatPassword : $scope.container.user.repeatPassword }
               }

               $http(req).then(
                (resp)=>{
                        if(resp.data.err){
                            dialog.modal('hide');
                            $scope.container.user.islogin = false;
                            $scope.container.user.password       = "";
                            $scope.container.user.repeatPassword = "";
                            bootbox.alert({
                                title: $scope.pageTitle = $filter('translate')(resp.data.err),
                                message: "<h1>" +$filter('translate')(resp.data.err)+"</h1>",
                                callback: function (result) {}
                                });

                        }else{
                                $scope.container.user.account             = resp.data.user.account;
                                $scope.container.user.wallet              = resp.data.user.wallet;
                                $scope.container.user.userid              = resp.data.user.userid;
                                $scope.container.user.Authorization       = "Bearer " + resp.data.token;
                                $scope.container.user.islogin             = true;
                                $scope.container.user.traderBalance       = {};
                                $scope.container.user.confirmBlockNumber  = {};
                                $scope.container.user.confirmedBlock      = {};
                                if( $scope.container.user.wallet ){
                                    $scope.container.user.wallet.balance     = {};
                                    $scope.container.user.wallet.ethGasprice = UTIL_CONF.ethGasprice;
                                    $scope.container.user.wallet.btcFee      = UTIL_CONF.btcFee;
                                }
                                //设定登录header
                                $http.defaults.headers.common['Authorization'] = $scope.container.user.Authorization;
                                //关闭Importing keystore弹窗
                                dialog.modal('hide');
                                //输出PDF文件
                                var doc = new jsPDF('letter')
                                doc.setFontSize(15);
                                var walletData = $scope.container.user.wallet
                                var dataText = []
                                var obj={}
                                obj["mnemonic"] = walletData["mnemonic"]
                                obj["bip39Seed"] = walletData["bip39Seed"]
                                for(var i in obj){
                                    var obj2 ={}
                                    obj2[i] = obj[i]
                                    dataText.push(obj2)
                                }
                                for(var j in dataText){
                                    dataText[j]=JSON.stringify(dataText[j]) +'\r'
                                    var len = dataText[j].length
                                    dataText[j] =  dataText[j].slice(1,len-2)
                                }
                                var lines = doc.splitTextToSize(dataText,280)
                                doc.text(10, 10,lines);
                                doc.save('KeyStore.pdf');

                                loadAssetList();
                                $localStorage.user = $scope.container.user;

                                bootbox.alert({
                                title: $scope.pageTitle = $filter('translate')("The wallet is created successfully"),
                                message: "<h1>" +$filter('translate')("You just created an Exchange Center account，please check the PDF keystore file")+"</h1>",
                                callback: function (result) {
                                    $window.location.href = $window.location.origin + '/index.html';
                                }
                                });
                        }
                },
                (err)=>{
                    console.log(err);
                });
        }

        $scope.showImportModal = function () {
            if ($scope.container.user.islogin) {
                systemInfo("One wallet existe，please clear wallet", "operation failed");
                return;
            }
            $scope.container.user.password = '';
            $scope.container.user.repeatPassword = '';
            $("#importModal").modal();
            getverifyCode();
        }

        $scope.getverifyCode = function () {
            getverifyCode()
        }

        function getverifyCode() {
            var captchapngId = new Date().getTime();
            var req = {
                        method: 'POST',
                        url: UTIL_CONF.middle_server + UTIL_CONF.CONSTANT.CAPTCHAPNG_URI,
                        data: { id: captchapngId }
                        }

                        $http(req).then(
                            (response)=>{
                            $scope.container.captchapng= response.data.image;
                            $scope.container.captchapngId = captchapngId;
                            },(err)=>{
                            console.log(err);
                            });

        }

        $scope.importWallet = function () {

            var req = {
                method: "POST",
                url: UTIL_CONF.middle_server + UTIL_CONF.CONSTANT.CAPTCHAPNG_URI + UTIL_CONF.CONSTANT.VERIFYIMG_URI,
                data: {
                        id: $scope.container.captchapngId,
                        code: document.getElementById("verify-input").value }
                    };
            $http(req).then(response => {
              if (response.data.error) {
                return systemInfo("Verifycode does not matched", "operation failed");
              }
              importWallet();
            });
        }

        function importWallet() {
            if ( ! $scope.container.user.wallet.mnemonic ){
                systemInfo("Please enter your wallet Mnemonic", "operation failed");
                return;
            }

            if ( ! $scope.container.user.password ){
                systemInfo("Please enter your wallet password", "operation failed");
                return;
            }

            if ( $scope.container.user.password.length < 8 ){
                systemInfo("Passwords must be at least 8 characters", "operation failed");
                return;
            }

            if ( $scope.container.user.password != $scope.container.user.repeatPassword ){
                systemInfo("Password does not equals repeated password", "operation failed");
                return;
            }

            var dialog = bootbox.dialog({
                    title   : $scope.pageTitle = $filter('translate')('Importing keystore'),
                    message : '<p><i class="fa fa-spin fa-spinner"></i>Importing keystore...</p>'
            });

            var req =   {
                            method: 'POST',
                            url: UTIL_CONF.middle_server + UTIL_CONF.CONSTANT.IMPORT_WALLET_URI,
                            data:  { mnemonic : $scope.container.user.wallet.mnemonic , password : $scope.container.user.password , repeatPassword : $scope.container.user.repeatPassword }
                        }

            $http(req).then((resp)=>{

                $scope.container.user.account             = resp.data.user.account;
                $scope.container.user.wallet              = resp.data.user.wallet;
                $scope.container.user.userid              = resp.data.user.userid;
                $scope.container.user.Authorization       = "Bearer " + resp.data.token;
                $scope.container.user.islogin             = true;
                $scope.container.user.confirmBlockNumber  = {};
                $scope.container.user.confirmedBlock      = {};
                $scope.container.user.traderBalance       = {};
                if( $scope.container.user.wallet ){
                    $scope.container.user.wallet.balance     = {};
                    $scope.container.user.wallet.ethGasprice = UTIL_CONF.ethGasprice;
                    $scope.container.user.wallet.btcFee      = UTIL_CONF.btcFee;
                }

                //设定登录header
                $http.defaults.headers.common['Authorization'] = $scope.container.user.Authorization;
                //关闭Importing keystore弹窗
                dialog.modal('hide');
                //输出PDF文件
                var doc = new jsPDF('letter')
                doc.setFontSize(15);
                var walletData = $scope.container.user.wallet
                var dataText = []
                var obj={}
                obj["mnemonic"] = walletData["mnemonic"]
                obj["bip39Seed"] = walletData["bip39Seed"]
                for(var i in obj){
                    var obj2 ={}
                    obj2[i] = obj[i]
                    dataText.push(obj2)
                }
                for(var j in dataText){
                    dataText[j]=JSON.stringify(dataText[j]) +'\r'
                    var len = dataText[j].length
                    dataText[j] =  dataText[j].slice(1,len-2)
                }
                var lines = doc.splitTextToSize(dataText,280)
                doc.text(10, 10,lines);
                doc.save('KeyStore.pdf');

                $localStorage.user                   = $scope.container.user;

                bootbox.alert({
                title: $scope.pageTitle = $filter('translate')("The wallet is created successfully"),
                message: "<h1>" +$filter('translate')("You just created an Exchange Center account，please check the PDF keystore file")+"</h1>",
                callback: function (result) {
                    $window.location.href = $window.location.origin + '/index.html';
                }
                });

                }, (err)=>{
                    dialog.modal('hide');
                            $scope.container.user.islogin = false;
                            $scope.container.user.password       = "";
                            $scope.container.user.repeatPassword = "";
                            bootbox.alert({
                                title: $scope.pageTitle = $filter('translate')(err),
                                message: "<h1>" +$filter('translate')(err)+"</h1>",
                                callback: function (result) {}
                      });
                });

        }

        $scope.outputAccount = function () {

            if ( !$scope.container.user.islogin ) {
                systemInfo("No wallet has been created yet", "operation failed");
                return;
            }

            bootbox.prompt({
                title: $scope.pageTitle = $filter('translate')("Please input Password"),
                inputType: 'password',
                callback: function (result) {
                    if (result) {
                        if( $scope.container.user.password == result )
                        {
                           toOutputAccount();
                        }
                        else
                        {
                            systemInfo("Password ERROR", "Please input correct password");
                        }

                    } else if (result == '') {
                        systemInfo("Please enter your wallet password", "operation failed");
                        return;
                    }

                }
            });
        }

        function toOutputAccount() {

            if ( ! $scope.container.user.wallet ) {
                systemInfo("Wallet is null, please import wallet or create wallet", "operation failed");
                return;
            }else{
                systemInfo( $scope.container.user.wallet.mnemonic , 'Please remember your mnemonic!' );

                var doc = new jsPDF('letter');
                doc.setFontSize(15);
                var walletData = $scope.container.user.wallet
                var dataText = []
                var obj={}
                obj["mnemonic"] = walletData["mnemonic"]
                obj["bip39Seed"] = walletData["bip39Seed"]
                for(var i in obj){
                    var obj2 ={}
                    obj2[i] = obj[i]
                    dataText.push(obj2)
                }
                for(var j in dataText){
                    dataText[j]=JSON.stringify(dataText[j]) +'\r'
                    var len = dataText[j].length
                    dataText[j] =  dataText[j].slice(1,len-2)
                }
                var lines = doc.splitTextToSize(dataText,280)
                doc.text(10, 10,lines);
                doc.save('KeyStore.pdf');
            }
        }

        function systemInfo(msg, title) {
            var bb = bootbox.confirm({
                title: $scope.pageTitle = $filter('translate')(title),
                message: $scope.pageTitle = $filter('translate')(msg),
                buttons: {

                },
                callback: function () {

                }
            });

            bb.bind('shown.bs.modal', function () {
                $('.modal-dialog').each(function (index, elem) {
                    $(elem).draggable({
                        handle: ".modal-header"
                    });
                });
            });
        };
        $scope.handleMenu = function(ev){
            $scope.isShow = true

        }
        $scope.handleMenu2 = function(ev){
            $scope.isShow = false

        }


    }
]);

homeModule.directive('headerShow', function () {
    return {
        templateUrl: 'home/header.html'
    };
});
homeModule.directive('marketList', function () {
    return {
        templateUrl: 'home/marketlist.html'
    };
});

homeModule.directive('assetList', function () {
    return {
        templateUrl: 'home/assetlist.html'
    };
});
homeModule.directive('footerShow', function () {
    return {
        templateUrl: 'home/foot.html'
    };
});

homeModule.directive('signup', function () {
    return {
        templateUrl: 'home/signup.html'
    };
});

homeModule.directive('transfer', function () {
    return {
        templateUrl: 'home/transfer.html'
    };
});

homeModule.directive('import', function () {
    return {
        templateUrl: 'home/import.html'
    };
});

homeModule.config(['$translateProvider', function ($translateProvider) {
    var lang = window.localStorage.lang;
    if ((lang != "en") && (lang != "zh_CN") && (lang != "ja") && (lang != "ko")) {
        lang = "zh_CN";
    }

    transEngine = $translateProvider;
    $translateProvider.useStaticFilesLoader({
        prefix: '/i18n/',///语言包路径
        suffix: '.json'
    });
    $translateProvider.preferredLanguage(lang);
}]);
