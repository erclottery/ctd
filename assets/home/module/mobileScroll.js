function fade() {
	document.getElementById("loading").style.display="none";
}
document.onreadystatechange = function () {
	if (document.readyState === "complete") {
			fade();
}}
mobileScroll(
					function(direction) {
						try {
							if (direction === 'down') {
								document.querySelector('.bottom-nav').classList.add('hidden')
							} else {
								document.querySelector('.bottom-nav').classList.remove('hidden')
							}
						} catch(err) {}
					}
			);
			function mobileScroll( fn ) {
				var beforeScrollTop = document.documentElement.scrollTop || document.body.scrollTop,
						fn = fn || function() {};
				window.addEventListener("scroll", function() {
					var afterScrollTop = document.documentElement.scrollTop || document.body.scrollTop,
							delta = afterScrollTop - beforeScrollTop;
					if( delta === 0 ) return false;
					fn( delta > 0 ? "down" : "up" );
					beforeScrollTop = afterScrollTop;
				}, false);
			}