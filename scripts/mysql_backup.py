#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import argparse
import commands
import sys
from datetime import datetime as dt

class MySQLDump:

    def __init__(self, options):
        self.dbconfig = options.dbconfig
        self.outdir   = options.outdir
        self.prefix   = options.prefix
        self.ignore_databases = ['information_schema', 'performance_schema', 'mysql', 'sys', 'test']

    def dump(self):
        self._check_outdir()

        databases = self._get_databases()

        for database in databases:
            if database not in self.ignore_databases:
                self._dump_database(database)

    def _check_outdir(self):
        if os.path.isdir(self.outdir) == False:
            raise Exception("Out directory '%s' not found." % self.outdir)

    def _get_databases(self):
        command = "mysql %s --silent -e 'SHOW DATABASES'" % self._get_options()
        print '[DEBUG]' + command
        result = commands.getstatusoutput(command)
        if result[0] > 0:
            raise Exception('Failed to fetch databases: ' + result[1]);

        return result[1].split("\n")

    def _dump_database(self, database):
        cur_time = dt.now()
        print "Dumping database %s..." % database
        filename = self.outdir + '/' + self.prefix + database + cur_time.strftime('%Y%m%d%H%M%S') + '.sql.gz'
        command = "mysqldump %s %s | gzip > %s" % (self._get_options(), database, filename)
        print '[DEBUG]' + command
        result = commands.getstatusoutput(command)
        if result[0] > 0:
            raise Exception(result[1])

        print filename

    def _get_options(self):
        options = []

        if self.dbconfig:
            options.append("--defaults-extra-file=%s" % self.dbconfig)

        return ' '.join(options)


def main():
    parser = argparse.ArgumentParser(description='Dump all mysql databases')
    parser.add_argument('-x', type=str, help='file prefix', default='mysql_', dest='prefix')
    parser.add_argument('outdir', type=str, help='directory to make dump files')

    args = parser.parse_args()

    args.dbconfig = os.path.dirname(os.path.abspath(__file__)) + "/ext_config.cnf"

    try:
        mysqldump = MySQLDump(args)
        mysqldump.dump()
    except Exception, e:
        print e
        exit(1)

if __name__ == "__main__":
    main()
